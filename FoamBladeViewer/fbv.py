#!/usr/bin/python

############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

# TODO

# Save last Pathes !!! 
# save last pathes in the dxf scanner, too.
# accept capital letters like .DXF
# Mouse interaction

import sys
import os
import os.path
import re
import string

from tkinter import *
from tkinter.filedialog import * 
from tkinter.messagebox import *

##print os.path.abspath('.')
##
##if os.path.exists( os.path.abspath('.')+'/../dxf2quad'):
##	sys.path.append(os.path.abspath('.')+'/../dxf2quad')
##	from TkInterface import *
##else:
##	print 'couldn\'t open dxf2quad library'
##	sys.exit()
##



class Quad(object):
	"""Shortened PathContainer Class"""
	#self.contentList=[]
	
	def writeToFile(self,filename):
		f=file(filename,'w+')
		os.chmod(filename,0o777) # octal nubmer -> leading 0
		if self.debug:
			print('writing data to "' +filename+'"')
			
		for i in self.contentList:
			f.write(  self.itemToString(i) +'\n')
		f.close()
		print('wrote data file "' +filename+'" with ' + str (len(self.contentList)) +' enteries')
		
	def itemToString(self,item):
		"""stolen from PathContainer"""
		s=str(round(item[0],4))+self.sep+str(round(item[1],4))+self.sep+\
			str(round(item[2],4))+self.sep+str(round(item[3],4))
		return s
		
	def stringToItem(self,s):
		if re.match(  '^(\s*-?\d+(\.\d+)?){4}',s):
			#p=re.split('\s',s,4)
			p=s.split()
			while '' in p:
				p.remove('')
			
			return [ float(p[0]),float ( p[1]),float(p[2]),float ( p[3])]
		else :
			return None
		

	def readFromFile(self,filename):
		"""stolen from PathContainer and adapted"""
		if self.debug:
				print('reading data from ' +filename)
		print(filename)
		self.filename=filename
		try:
			self.file=open(filename)
		except:
			print('Couldn\'t open file')
			return None
		
		self.contentList=[]
		self.info=''
		j=0
		for i in self.file:
			#try:
			item=self.stringToItem(i)
			if	  item:
				self.contentList.append(item)
			#except:
			else:
				if not re.match('\S+',i):
					pass #only spaces
				else:
					print('\tignored line: '+str(j) +'"' + i.strip() +'"')
					self.info+='INFO: ' +i.strip()
			j+=1	
		
		self.amount=len(self.contentList)
		#print '\tread data file '+ self.filename+' with ' + str (self.amount) +' enteries'
		return True #success

	def __init__(self,src):
		self.sep='\t\t'	
		self.debug=False
		self.contentList=None
		self.amount=None
		if type(src)==list:
			self.contentList=src
			self.amount=len(src)
		else:
			ret=self.readFromFile(src)
			if ret==None:
				#return None
				raise IOError
##		
###stolen from Path container
##def scanDXF(filename):
##	"""uses dxf2quad to get a quad"""
##	##not very beautiful
##	os.system(' rm -f Shapes/path.temp')
##	path='../dxf2quad/dxf2quad.py '
##	args= filename+ ' Shapes/path.temp'
##	try:
##		#os.system(path+args)
##		os.system("rxvt -e "+path+args)
##	except:
##		print 'fbv.py Couldn\'t open file'
##		return None
##	return Quad('Shapes/path.temp')			

def scanDXF_Tk():
	"""uses dxf2quad to get a quad"""
	##not very beautiful
	
	if sys.platform=='linux2':
		os.system( 'rm -f temp.quad')	#das mag windows nicht
	else:
	#if sys.platform=='linux2':
		#os.system( 'rm -f temp.quad')	#das mag windows nicht
		showinfo('INFO', 'Cannot erase temporary file on %s'%sys.platform)
		pass
	#showinfo('INFO', 'erased temporary file on %s'%sys.platform)
	
	#verbessern
	path='..'+os.sep+'dxf2quad'+os.sep+'TkInterface.py '
	#path='.'+os.sep+'dxf2quad'+os.sep+'TkInterface.py '	#for fbv releases
	
	
	
	args= 'temp.quad'
	try:
		execstring=sys.executable +' '+path+args
		print(execstring)
		os.system(execstring)
		#os.system("rxvt -e "+path+args)
	except:
		#print 'fbv.py: Couldn\'t open file'
		showerror('ERROR','Failure in Calling the dxf2quad')
		return None
	try:	
		quad=Quad('temp.quad')			
	except:
		return None
	return quad
##
##def scanDXF_Tk():
##	"""uses dxf2quad to get a quad"""
##	##not very beautiful
##
##	#try:
##	quad=(dxf2quadTk()) #no instance of class Quad
##	print 'returned from scanning'
##	#except:
##	#	print 'Couldn\'t open file'
##	#	return None
##	return quad

class FBViewer(object):

	def scalepos(self):
		#print 'scale= %f'%self.scaleval
		self.scaleval*=self.scaleamount
##		self.canvas.scale( ALL, 0.0, 0.0, 1.2, 1.2 ) 
		self.redraw()
		self.updatesidebar()
		self.updatewire()

	def scaleneg(self):
		#print 'scale= %f'%self.scaleval
		self.scaleval*=1/self.scaleamount
##		self.canvas.scale( ALL, 0.0, 0.0, -1.2, -1.2 ) 
		self.redraw()
		self.updatesidebar()
		self.updatewire()
	
	def scaleneutral(self):
		#print 'scale= %f'%self.scaleval
		self.scaleval=self.pixelmm
##		self.canvas.scale( ALL, 0.0, 0.0, -1.2, -1.2 ) 
		self.redraw()
		self.updatesidebar()
		self.updatewire()
		
		
	def redraw(self):
		#print 'redraw'
		self.updatewire()
		self.drawQuad()
	def updatesidebar(self):

		#updated by friends
		#self.sidetext1='hallo'
		
		
		coords='Coordinates:\nx: %4.2f y: %4.2f\nu: %4.2f v: %4.2f'%	(self.valuelist[0][self.pos],
				self.valuelist[1][self.pos],
				self.valuelist[2][self.pos],
				self.valuelist[3][self.pos])
			
		self.sidetext2=coords
		
		self.sidetext3='Scale factor:\n%4.2f'%self.scaleval
		
		self.sideLabel1.config(text=self.sidetext1)
		self.sideLabel2.config(text=self.sidetext2)
		self.sideLabel3.config(text=self.sidetext3)
	
	def updatewire(self):
	
			if self.quad.contentList==None:
				return

			self.cleanwire()
			
			
			self.wire=self.canvas.create_line(
				self.valuelist[0][self.pos]*self.scaleval,
				self.valuelist[1][self.pos]*self.scaleval,
				self.valuelist[2][self.pos]*self.scaleval,
				self.valuelist[3][self.pos]*self.scaleval, 
				fill='green')
			self.cross1=self.canvas.create_line(
				self.valuelist[0][self.pos]*self.scaleval+5,
				self.valuelist[1][self.pos]*self.scaleval+5,
				self.valuelist[0][self.pos]*self.scaleval-5,
				self.valuelist[1][self.pos]*self.scaleval-5, 
				fill='green')
			self.cross2=self.canvas.create_line(
				self.valuelist[0][self.pos]*self.scaleval-5,
				self.valuelist[1][self.pos]*self.scaleval+5,
				self.valuelist[0][self.pos]*self.scaleval+5,
				self.valuelist[1][self.pos]*self.scaleval-5, 
				fill='green')
			self.cross3=self.canvas.create_line(
				self.valuelist[2][self.pos]*self.scaleval+5,
				self.valuelist[3][self.pos]*self.scaleval+5,
				self.valuelist[2][self.pos]*self.scaleval-5,
				self.valuelist[3][self.pos]*self.scaleval-5, 
				fill='green')
			self.cross4=self.canvas.create_line(
				self.valuelist[2][self.pos]*self.scaleval-5,
				self.valuelist[3][self.pos]*self.scaleval+5,
				self.valuelist[2][self.pos]*self.scaleval+5,
				self.valuelist[3][self.pos]*self.scaleval-5, 
				fill='green')
		
	def cleanwire(self):
			self.canvas.delete(self.wire)
			self.canvas.delete(self.cross1)
			self.canvas.delete(self.cross2)
			self.canvas.delete(self.cross3)
			self.canvas.delete(self.cross4)
	
	def scalemoved(self,argstr):
		#print type(arg)
		self.pos=int(argstr)
		if self.quad:
			#self.redraw()

			self.cleanwire()
			self.updatewire()
			self.updatesidebar()
			
	
	def openQuad(self):
		
		name=askopenfilename()
		if name:
			if name[-5:]=='.quad':	
				self.quad=Quad(name)
				self.drawQuad()
				self.simscale.config(to=self.quad.amount-1)
				self.sidetext1='lines: %i'%self.quad.amount
				self.updatewire()
			else:
				showerror('ERROR', 'Please select a "*.quad" file')				
	
	def openQuadFile(self,filename):
		if filename[-5:]=='.quad':
			self.quad=Quad(filename)
			self.drawQuad()
			self.simscale.config(to=self.quad.amount-1)	
			self.sidetext1='lines: %i'%self.quad.amount
		else:
			showerror('ERROR', 'Please select a "*.quad" file')			
	
	def openDXF(self):
		#name=askopenfilename()
		self.quad= scanDXF_Tk()
		
		#print self.quad
		if self.quad:
			self.drawQuad()
			self.simscale.config(to=self.quad.amount-1)
			self.sidetext1='lines: %i'%self.quad.amount

		else:
			showerror('ERROR', 'An Error occoured while opening the DXF')	
			print('Error occoured while opening DXF')


	##todo			
	
	def saveQuad(self):
		filename=asksaveasfilename()
		if filename[-5:]=='.quad':
			self.quad.writeToFile(filename)
		else:
			showerror('ERROR', 'Please choose a "*.quad" file')
	
	def quickopenQuad(self):
		self.quad=Quad('../FoamBlade/Shapes/clarky.quad')
		print(self.quad)
		self.drawQuad()
		self.simscale.config(to=self.quad.amount-1)
	
			
	def simplus(self):
		self.pos=self.simscale.get()+1
		self.simscale.set(self.pos)
		if self.quad:
			#self.redraw()
			self.cleanwire()
			self.updatewire()
	
	def simminus(self):
		self.pos=self.simscale.get()-1
		self.simscale.set(self.pos)
		if self.quad:
			#self.redraw()

			self.cleanwire()
			self.updatewire()
	
	def toggleorgin(self):
	
		if self.org1 and self.org2:
			self.canvas.delete(self.org1)
			self.org1=None
			self.canvas.delete(self.org2)
			self.org2=None
		else:
			self.org1=self.canvas.create_line(-10,0,10,0, fill='white')
			self.org2=self.canvas.create_line(0,-10,0,10, fill='white')
		
	def togglegrid(self):
		showinfo('INFO', 'implement me')
		print('implement me')
		
	
	def drawQuad(self):
		#adapted from the original fbviewer
		
		if self.quad.contentList==None:
			return
		elif len(self.quad.contentList)==0:
			return
		self.viewList=[]
		self.viewList[:]=self.quad.contentList
		
		height=self.canvas.winfo_height()
		width=self.canvas.winfo_width()

		xylist=[]	#in scaled coords
		uvlist=[]
		
		self.valuelist=([],[],[],[])
			
		for i in self.viewList:
			xylist.append( i[0]*self.scaleval )	#for create_line
			xylist.append( -i[1]*self.scaleval )
			uvlist.append( i[2]*self.scaleval )
			uvlist.append( -i[3]*self.scaleval )
			
			self.valuelist[0].append( i[0]) #separate all values
			self.valuelist[1].append( -i[1])
			self.valuelist[2].append( i[2])
			self.valuelist[3].append( -i[3])
			
			
		#self.canvas.create_line(xy, fill='green') #cool
	
		#measure dimensions
		xmax=max(self.valuelist[0])*self.scaleval
		ymax=max(self.valuelist[1])*self.scaleval
		umax=max(self.valuelist[2])*self.scaleval
		vmax=max(self.valuelist[3])*self.scaleval
		
		xmin=min(self.valuelist[0])*self.scaleval
		ymin=min(self.valuelist[1])*self.scaleval
		umin=min(self.valuelist[2])*self.scaleval
		vmin=min(self.valuelist[3])*self.scaleval

		objectx=int(xmax+abs(xmin))
		objecty=int(ymax+abs(ymin))
		objectu=int(umax+abs(umin))
		objectv=int(vmax+abs(vmin))

		b=50 #borderwidth

		bbox=( min(xmin,umin)-b, min(ymin,vmin)-b, max(xmax,umax)+b, max(ymax,vmax)+b) 
		#print 'object size in pixel: ' + str(objectx) + '  ' + str(objecty)
		
		
		self.canvas.config ( scrollregion=bbox )		

		self.canvas.delete( self.xyline)
		self.canvas.delete( self.uvline)
		
		self.xyline=self.canvas.create_line(xylist, fill='red') #cool 
		self.uvline=self.canvas.create_line(uvlist, fill='blue') #cool

			
	def __init__(self):
		self.scaleamount=1.2
		self.quad=None
		self.wire=None
		self.cross1=None
		self.cross2=None
		self.cross3=None
		self.cross4=None

		self.org1=None
		self.org2=None
		
		self.pixelmm=5	#pixel per mm by scale=1
		self.scaleval=self.pixelmm
		
		self.xyline=None
		self.uvline=None

		self.pos=0
		
		## Begin Gui Construction
		self.rootwin=Tk(className="FoamBlade Viewer")
		#grid Layout 
		#3 columns 5 rows
		
		
		self.canvas=Canvas(self.rootwin,bg='black',height=400,width=600,scrollregion=(-200,-200,200,200) )
		
		self.menubar=Menu(self.rootwin,relief=GROOVE)
		self.menubar.add_command(label='Open Quad',command=self.openQuad)
		self.menubar.add_command(label='Scan DXF',command=self.openDXF)
		self.menubar.add_command(label='Save Quad',command=self.saveQuad)
		
		self.rootwin.config(menu=self.menubar)
		
		self.empty=Frame(master=self.rootwin,bg='blue')
		self.empty.grid(column=0,row=0, columnspan=3,sticky=NW)
		
		
		self.toolbar=Frame(master=self.rootwin,bg='blue')
		self.toolbar.grid(column=0,row=1, columnspan=3,sticky=NW)
		
##		self.toolbutton1=Button(self.toolbar,text='test1', command=self.testcommand)
##		self.toolbutton1.pack(fill=BOTH, side=LEFT)
		self.toolbutton2=Button(self.toolbar,text='redraw',command=self.redraw)
		self.toolbutton2.pack(fill=BOTH, side=LEFT)
		self.toolbutton3=Button(self.toolbar,text='scale +',command=self.scalepos)
		self.toolbutton3.pack(fill=BOTH, side=LEFT)
		self.toolbutton4=Button(self.toolbar,text='scale -',command=self.scaleneg)
		self.toolbutton4.pack(fill=BOTH, side=LEFT)
		self.toolbutton5=Button(self.toolbar,text='scale 1:1',command=self.scaleneutral)
		self.toolbutton5.pack(fill=BOTH, side=LEFT)

		self.toolbutton6=Button(self.toolbar,text='orgin',command=self.toggleorgin)
		self.toolbutton6.pack(fill=BOTH, side=LEFT)
		self.toolbutton6=Button(self.toolbar,text='grid',command=self.togglegrid)
		self.toolbutton6.pack(fill=BOTH, side=LEFT)
		
		self.sidebar=Frame(master=self.rootwin)
		self.sidebar.grid(column=0,row=2,rowspan=2,sticky=N)
		
		self.sidetext1='<daten>'
		self.sidetext2='<coods>'
		self.sidetext3='<scale>'
		
		self.sideLabel1=Label(self.sidebar,text=self.sidetext1,width=20,relief=RIDGE)#wraplength=20
		self.sideLabel1.pack(fill=BOTH,side=TOP)
		self.sideLabel2=Label(self.sidebar,text=self.sidetext2,width=20,relief=RIDGE)#wraplength=20,width=25)
		self.sideLabel2.pack(fill=BOTH,side=TOP)
		self.sideLabel3=Label(self.sidebar,text=self.sidetext3,width=20,relief=RIDGE)#wraplength=20,width=25)
		self.sideLabel3.pack(fill=BOTH,side=TOP)
		
		#print os.path.abspath(sys.argv[0])
		self.dirname=os.path.dirname(os.path.abspath(sys.argv[0]))
		if os.path.isfile(self.dirname + os.sep+'Coord-systems.gif'):
			print(os.path.curdir)
			self.sidePicture=PhotoImage(file=self.dirname + os.sep+'Coord-systems.gif')
			self.picLabel=Label(self.sidebar,image=self.sidePicture,relief=RIDGE)
			self.picLabel.pack(fill=BOTH,side=TOP)
		
		#canvas=Canvas(self.rootwin,bg='black')
		self.canvas.grid(column=1,row=2,sticky=N+E+S+W) 
		#later more
		
		self.vscrollbar=Scrollbar(orient=VERTICAL,command=self.canvas.yview)
		self.vscrollbar.grid(column=2,row=2,sticky=N+S)
		
		
		self.hscrollbar=Scrollbar(orient=HORIZONTAL,command=self.canvas.xview)
		self.hscrollbar.grid(column=1,row=3,sticky=E+W)
		
		self.canvas.config(xscrollcommand=self.hscrollbar.set,yscrollcommand=self.vscrollbar.set)
		
		simvalue=IntVar()
		self.simscale=Scale(orient=HORIZONTAL,length=100,command=self.scalemoved,
			to=100,variable=simvalue)
		self.simscale.grid(column=1,row=4,sticky=E+W)
		
		self.scaleplus=Button(self.rootwin,text='+',command=self.simplus)
		self.scaleplus.grid(column=2,row=4)
		self.scaleminus=Button(self.rootwin,text='-',command=self.simminus)
		self.scaleminus.grid(column=0,row=4)

		
		
		#adjust grid
		#neccessary to be able to resize the window dynamically
		self.rootwin.columnconfigure(1,weight=1)
		self.rootwin.rowconfigure(2,weight=1)
		
		self.toggleorgin()
		
		if len(sys.argv)>1:
			print(sys.argv)
			if (sys.argv[1][-5:]).lower()=='.quad':
				self.openQuadFile(sys.argv[1])
				
				print("File length: " +str(len(self.quad.contentList)))
				if len(self.quad.contentList)==0 or self.quad==None:
					#do something better
					#here!
					showinfo('INFO', 'The File has no useable content')
					print('The File has no useable lines')
					return None
					
			if (sys.argv[1][-4:]).lower() =='.dxf':
				scanDXF_Tk(sys.argv[1])
				
				
		#else:
		#	if os.path.exists(self.dirname+os.sep+'FBCircle.quad'):
		#		self.openQuadFile(+os.sep+'FBCircle.quad')
		
if __name__=='__main__':
	fbv=FBViewer()
	fbv.rootwin.mainloop()
