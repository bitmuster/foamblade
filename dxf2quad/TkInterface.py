#!/usr/bin/python

############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

from tkinter import *
from dxf2quad import *
import MessageWrapper
from MessageWrapper import *
import sys
import os.path

import string

from tkinter.filedialog import * 
from tkinter.messagebox import *

#away with them
#ppsg_var=None
filename=None
tkquad=None

def dxf2quadTk(outfile):
	global filename
	#global ppsg_var
	global tkquad
	
	def openbutton():
		global filename
##		if len(sys.argv)>=2:
##			filename=sys.argv[1]
##		else:
		filename=askopenfilename()

		if string.lower(filename[-4:])=='.dxf':
			if filename:		
				textfield.insert(END, 'Selected file: ' + filename+ '\n')
			else:
				textfield.insert(END, 'Couldn\'t open file\n')
				showerror('ERROR', 'Couldn\'t open file')
		else:
			showerror('ERROR', 'Please choose a "*.dxf" file')
			filename=None
		textfield.insert(END, 'To Continue press Start\n')

		#print 'ppsg '+ str(ppsg_var.get())
				
	def startbutton():
		global filename
		
		if filename ==None:
			showerror('ERROR', 'Please open a *.dxf file')
			return 
		textfield.insert(END, 'Processing please wait...\n')
			
		#global ppsg_var
	#	try:
		#print filename
		#print 'kvar %i'%kvar.get()
		#print type(ppsg_var.get())
		#print 'ppsg '+ str(ppsg_var.get())
		
		#pp=int(ppsg_var.get())
		ppmm=float(entry.get())
		
		#print 'pp %i'%pp
		#except:
#fix:		textfield.insert(END, 'Couldn\'t read value: "'+ppsg_var.get()+'"\n')
		#return
		textfield.config(state=NORMAL)
		textfield.insert(END, 'Started, please wait this could take a while\n')
		
		#dxf2quad('paths/allObjects.dxf',pp,debug=kvar.get())
		tkquad=dxf2quad(filename,outfile,ppmm,debug=kvar.get())
		toolbuttonstart.config(default=DISABLED)
		
		#startbutton.config(default=NORMAL)
		#toolbuttonstart.destroy()
		textfield.insert(END, '\nFinished\n')		
		textfield.see(END)
		
		toolbar.destroy() # disable config

		#textfield.insert(END, '\n(editing disabled)\n')		
		textfield.config(state=DISABLED)

		
	def savebutton():
	
		#filename=asksaveasfilename()
		showinfo('INFO', 'implement me')
		textfield.insert(END, 'Not implemented: saving to: %s'%filename)
		
		
##	def okbuttoncmd():
##		print 'Ok'
##		tkwin.destroy()
		
	def exitbutton():
		tkwin.destroy()
		sys.exit()
	
	####


		
	tkwin=Tk(className='DXF Scanner dxf2quad')
	kvar=IntVar()
	ppsg_var=StringVar()

	toolbar=Frame(master=tkwin)
	toolbar.grid(column=0,row=0, columnspan=1,sticky=NW)
	
	toolbuttonopen=Button(toolbar,text='Open DXF',command=openbutton)
	toolbuttonopen.pack(fill=BOTH, side=LEFT)	
	toolbuttonstart=Button(toolbar,text='Start',command=startbutton)
	toolbuttonstart.pack(fill=BOTH, side=LEFT)	
	
	toolbutton3=Checkbutton(toolbar,text='Debug',variable=kvar)
	toolbutton3.pack(fill=BOTH, side=LEFT)	
	toolbutton3.deselect()
	

	
	label=Label(toolbar, text='points per MILLIMETER\n (recommended 0.1 ... 50):')
	#label=Label(toolbar, text='points per Subgraph:')
	label.pack(fill=BOTH, side=LEFT)	
	entry=Entry(toolbar, textvariable=ppsg_var)
	entry.pack(fill=BOTH, side=LEFT)	
	
	entry.insert(0,10)
	#print 'start ppsg '+ str(ppsg_var.get())



	
	textfield=Text(tkwin)
	textfield.grid(column=0,row=1,sticky=N+E+S+W)
	
	scrollbar=Scrollbar(tkwin,
	orient=VERTICAL,command=textfield.yview)
	scrollbar.grid(column=1,row=1,sticky=N+S)
	
	textfield.config(yscrollcommand=scrollbar.set,takefocus=0 )
	#textfield.unbind_all ( "Activate" )
	#textfield.unbind_all ( "Any-KeyPress" )
	#textfield.config(state=DISABLED)

	MessageWrapper.msgfunc=lambda arg:textfield.insert(END,arg)
	
	exitbar=Frame(master=tkwin)
	exitbar.grid(column=0,row=3, columnspan=1)
	
##	okbutton=Button(exitbar,text='Ok',command=okbuttoncmd)
##	okbutton.pack(fill=BOTH, side=LEFT)	
	quitbutton=Button(exitbar,text='Quit',command=exitbutton)
	quitbutton.pack(fill=BOTH, side=LEFT)	
	
##	toolbuttonstart=Button(exitbar,text='Save',command=savebutton)
##	toolbuttonstart.pack(fill=BOTH, side=LEFT)	
	
	tkwin.columnconfigure(0,weight=1)
	tkwin.rowconfigure(1,weight=1)
	
	textfield.insert(END, 'DXF2Quad 0.03 Copyright Michael Abel 2007\n')
	textfield.insert(END, 'Released under the GNU GENERAL PUBLIC LICENSE\n')
	textfield.insert(END, '\n')
	textfield.insert(END, 'Selected Output %s \n'%outfile)
	textfield.insert(END, '\n')
	textfield.insert(END, 'To Continue Open a DXf File\n')

	#print'mainloop'
	tkwin.mainloop()
	#print'leaving mainloop'
	
	#textfield.config(state=DISABLED)
	#finished 
	return tkquad

if __name__=='__main__':
	print('starting Tk interface')


	
	if len(sys.argv) != 2	 :
		xpath=sys.path[0] # conatins the executed script
		outfile=os.path.join(xpath,'output.quad')
		print('using outpufile %s'%outfile)
		#print 'too less arguments'
		#sys.exit(-1)
	else	:
		outfile=sys.argv[1]
	#print outfile	
	dxf2quadTk(outfile)
	


