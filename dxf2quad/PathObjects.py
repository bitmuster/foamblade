
############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

import math
import sys

from dxf_fb import *
from dxf_codes import *

from MessageWrapper import *

#missing
#polyline
#lwpolyline
#spline

degrad=math.pi/180
raddeg=180/math.pi

debug=2;

class PathObject(object):
	"""Object that can occour in a parsed path
	"""
	def __init__(self):
		pass
	def getPoints(self):
		return self
	
class Point(PathObject):
	"""A point 
	currently the z-Axis is partly ignored
	"""
	precision=4 # 10**-4
	epsilon=10**-precision	#Class valiable use with self.epsilon

	def __init__(self,x=0,y=0,z=0,color=0,layer=0):
		self.x=float(x)
		self.y=float(y)
		self.z=float(z) # not set in some functions
		self.color=color
		self.layer=layer
		
	
	def __str__(self):
		s= '(%4.1f,%4.1f)' %(float(self.x),float(self.y))
		return s
	
	def __eq__(self,b):

		#use just an rectangular window
		if ( self.x-self.epsilon <= b.x ) and \
			( b.x <=self.x+self.epsilon) and \
			(self.y-self.epsilon <=b.y) and \
			(b.y <=self.y+self.epsilon):
			return True
		else:
			return False


class ExtendedPathObject(PathObject):
	"""abstract
	Objects that have a begin an an end e.g. lines, arcs...
	"""
	def __init__(self):
		PathObject.__init__(self)
	def getLength(self):	
		print('abstract!')
		sys.exit(-1)
	def interpolate(self,points):
		print('abstract!')
		sys.exit(-1)
	def getNPoints(self,n):
		"""retuns N equaldistanced points from start to end on the object
		"""
		print('abstract! getNPoints')
		sys.exit(-1)
		
	def reverse(self):
		print('abstract! reverse')
		sys.exit(-1)

	def getEndPoints(self):
		return (self.points[0],self.points[-1])
	def getOtherEnd(self,point):
		if point==self.points[0]:
			return self.points[-1]
		elif point==self.points[-1]:
			return self.points[0]
		else:
			message( 'cannot get end: point is not part of the object')
			return None

	def setBegin(self,point):
		if point==self.points[0]:
			return self
		elif point==self.points[-1]:
			#self.points=(self.points[-1],self.points[0])
			self.reverse()
			return self
		else:
			message( 'cannot set beginning: point is not part of the object')
			message( point)
			message( self)
			return None
	def setEnd(self,point):
		if point==self.points[-1]:
			return self
		elif point==self.points[0]:
			#self.points=(self.points[-1],self.points[0])
			self.reverse()
			return self
		else:
			message( 'cannot set beginning: point is not part of the object')
			message( point)
			message( self)
			return None

class MultiPathObject(ExtendedPathObject):
	"""abstract
	Objects consist of PathObjects -> POLYLINES
	"""
	def __init__(self):
		ExtendedPathObject.__init__(self)
		self.objects=None
		
	
	def getObjects(self):
		print('abstract!')
		sys.exit(-1)

		
class Line(ExtendedPathObject):
	def __init__(self,x0=0,y0=0,z0=0,x1=0,y1=0,z1=0,color=0,layer=0):
		ExtendedPathObject.__init__(self)
		self.points=( Point(x=x0,y=y0,z=z0) , Point(x=x1,y=y1,z=z1) )
		#in the order bengin...end
		self.color=color
		self.layer=layer
	def getLength(self):
		return 	math.sqrt( 
				(self.points[-1].x-self.points[0].x)**2+
				(self.points[-1].y-self.points[0].y)**2
				)
				#+(self.points[1].z-self.points[0].z)**2 )

	def __str__(self):
		s='Line : '+ str(self.points[0]) + '->' + str(self.points[1])
		return s

	def getNPoints(self,nf):
		"""retuns N equidistanced points from start to end-1 on the object
		"""
		n=int(round(nf))
		l=self.getLength()
		step=float(l)/(n) # hier kein n-1
		xdir_n=float(self.points[-1].x-self.points[0].x)/l
		ydir_n=float(self.points[-1].y-self.points[0].y)/l
		
		#message( " len %i step %f xdir %f ydir %f"%(l,step,xdir_n,ydir_n))
		
		li=[]
		for i in range(n):
			xp=xdir_n*step*i+self.points[0].x
			yp=ydir_n*step*i+self.points[0].y
			po=Point(xp,yp)
			#message( po)
			li.append(po  )
		return li
	
	def reverse(self):
		self.points=(self.points[-1],self.points[0])
			
class Arc(ExtendedPathObject):
	def __init__(self,x0=0,y0=0,z0=0,radius=0,angle0=0,angle1=0,color=0,layer=0):
		ExtendedPathObject.__init__(self)
		#self.points=( Point(x=x0,y=y0,z=z0) , Point(x=x1,y=y1,z=z1) )
		#in the order bengin...end
		self.origin=Point(x0,y0,z0)
		self.radius=radius
		self.angle0=angle0	#degree
		self.angle1=angle1
		self.color=color
		self.layer=layer	
		self.points=[]
		
		self.reversemarker=0
		
		self.points=[\
			Point(x0+self.radius*math.cos(math.pi/180 * self.angle0),\
				 y0+	self.radius*math.sin(math.pi/180 * self.angle0)),\
			Point(x0+self.radius*math.cos(math.pi/180 * self.angle1),\
				 y0+	self.radius*math.sin(math.pi/180 * self.angle1))	]
				 
		if debug>0:
			message( self.strall())

	def getLength(self):


		if debug>0:
			message('angle0 %f angle1 %f'% (self.angle0,self.angle1))
		if self.angle0 < self.angle1:
			l= self.radius* degrad* abs( self.angle0-self.angle1)
		else: # bow  passes zero
			
			l= self.radius* degrad* ( 360-self.angle0+self.angle1) 
			
		if debug>0:
			message('length of arc is %f radius is %f'% (l, self.radius))
		return l
		
	def strall(self):
		s='Arc: origin %s radius %6.2f angles:%6.2f,%6.2f'\
			%(str(self.origin),float(self.radius),float(self.angle0),float(self.angle1))
		return s

	def __str__(self):
		s='Arc  : ' +str(self.points[0]) + '->' + str(self.points[-1])
		return s

	def getNPoints(self,nf):
		"""retuns N equidistanced points from start to end-1 on the object
		"""
			
		n=int(round(nf))
		if self.angle1 > self.angle0: #ok
			bow=self.angle1-self.angle0
		else:#bow passes over zero
			bow=(360-self.angle0)+self.angle1 #+math.pi
		
		step=float(bow)/n
		li=[]
		if debug>0:message( "bow %f step %f angle0 %f angle1 %f"%(bow,step,self.angle0,self.angle1))
		if self.reversemarker==0: # is important since we leave the last object out
			for i in range(n):
					po=Point(self.origin.x+self.radius*math.cos(degrad * (self.angle0+step*i)),\
						self.origin.y+	self.radius*math.sin(degrad * (self.angle0+step*i)))
					li.append(po)
		else:
			for i in range(n):
					po=Point(self.origin.x+self.radius*math.cos(degrad * (self.angle1-step*i)),\
						self.origin.y+	self.radius*math.sin(degrad * (self.angle1-step*i)))
					li.append(po)
		return li

	def reverse(self):
		self.points=(self.points[-1],self.points[0])
		if self.reversemarker==0:
			self.reversemarker=1
		else:
			self.reversemarker=0
		
		
class Ellipse(ExtendedPathObject):
	def rePi(self, val):
		"""Reduce angle to 0..2pi"""
		while val>= 2*math.pi:
			val-=2*math.pi
		while val<0:
			val+=2*math.pi
		return val
	
	def __init__(self,
			xcenter=0,
			ycenter=0,
			zcenter=0,
			end_rel_major_x=0,
			end_rel_major_y=0,
			end_rel_major_z=0,
			ratio=0,
			start_parameter=0,
			end_parameter=0,
			color=0,
			layer=0):
		ExtendedPathObject.__init__(self)
		#values from the dxf-file		
		self.origin=Point(xcenter,ycenter,zcenter)
		self.ratio=ratio # major vs minor axis
		self.color=color
		self.layer=layer	
		self.start_E=self.rePi(start_parameter) #relative to ellipse system; in radiants
		self.end_E=self.rePi(end_parameter)
		self.end_rel_major_x=end_rel_major_x
		self.end_rel_major_y=end_rel_major_y
		
		#generate vector for minor axis from major axis
		self.end_rel_minor_y,self.end_rel_minor_x=self.end_rel_major_x*self.ratio,-self.end_rel_major_y*self.ratio 
		
		self.major=math.sqrt(self.end_rel_major_x**2+self.end_rel_major_y**2) # Betrag 1. Hauptachse (major)
		self.minor=math.sqrt(self.end_rel_minor_x**2+self.end_rel_minor_y**2) # Betrag 2. Haputachse (minor)

		self.angle0=2*math.pi-math.atan2(self.end_rel_major_y,self.end_rel_major_x) #ccw definition # angle major
		#while self.angle0>=2*math.pi:
		#	self.angle0-=2*math.pi
		self.angle0=self.rePi(self.angle0)	
		self.angle1=2*math.pi-math.atan2(self.end_rel_minor_y,self.end_rel_minor_x) #ccw definition # angle minor
		#while self.angle1>=2*math.pi:
		#	self.angle1-=2*math.pi
		self.angle1=self.rePi(self.angle1)	
		
		self.points=[self.calcPoint(self.start_E),
			self.calcPoint(self.end_E)]
		
		self.length=self.approxLen()
		
		self.reversemarker=0
		if debug>0:
			message( self.strall()	)
			
	def rotate2D(point,alpha,org):
		#alpha in radiants
		#M=( cos,-sin)
		#    sin,cos
		#mirror
		alpha=alpha
		#rotate
		xnew=(point.x-org.x)*math.cos(alpha)-(point.y-org.y)*math.sin(alpha) + org.x
		ynew=(point.x-org.x)*math.sin(alpha)+(point.y-org.y)*math.cos(alpha) + org.y
		return Point(xnew,ynew,0)
	rotate2D=staticmethod(rotate2D)
		
	def calcPoint(self,angleG):
		"""eats an angle in radiants
		angle is relative to the Ellipse system
		"""
		#first we calc the value on an unrotated ellipse 
		#rotate it with the main axis angle to get the right point

			

		#transform from global to ellipse system
		#aE=(-self.angle0+angleG) 
		aE=angleG #No!

		if (aE < 0) or ( aE > 360*degrad):
			message("Warning ellipse parameter out of range, trying to repair...")
			aE=self.rePi(aE)

		scaledtan=float(self.major)/self.minor * math.tan(aE) 
			#message( "%4f %4f"%(raddeg *math.tan(b),raddeg*b ) )
		if 90*degrad<aE<=270*degrad:
			a=math.atan( scaledtan )+math.pi
			#message( 'adding')
		else:
			a=math.atan(scaledtan)
			
		pbetha=Point( self.major*math.cos (aE)+self.origin.x, self.minor*math.sin(aE)+self.origin.y,0)
		
		#transform to global system
		p=self.rotate2D(pbetha,-self.angle0,self.origin) 
		#message( "system :%4f ellipse: %4f coordE %4f %4f coordG %4f %4f " )
		#	%(raddeg*angleG,raddeg*aE,pbetha.x,pbetha.y,p.x,p.y)
		return p

	def getLength(self):
		return self.length
		
	def approxLen(self):
		"""Tune me, check me I#
		"""
		#message( 'debug:  ' + str(debug))
		#if debug>0:
		##for an ellipse with x^2/a^2+y^2/b^2=1
		##Perimeter:
		##U=int_startphi^endphi 
		##
		##We do this here in the ellipse system 
		if debug>0:message( 'Approximating Ellipse length... ')
		if debug>0:message( 'Start radiant: %f, end radiant: %f ' %(self.start_E,self.end_E))
		if debug>0:message( 'Start degree: %f, end degree: %f ' %(self.start_E*180/math.pi,self.end_E*180/math.pi))
		
		spr=0.0001	# step in radiants
		#message( self.start_E*1/spr,self.end_E*1/spr)
		if self.start_E > self.end_E:
			offset=2*math.pi
		else:
			offset=0
		
		l=0
		for phi_m in range(int(self.start_E*1/spr),int((self.end_E+offset)*1/spr)):
			k=spr*math.sqrt( 
				self.major**2*math.sin(phi_m*spr)**2+
				self.minor**2*math.cos(phi_m*spr)**2 ) 
			#message( 'segment length %f' % k )
			l+=k
			
		if debug>0:message( 'Ellipse length %f' % l )
		return l
	

		
	def strall(self):
		s='Ellipse: origin %s ratio %6.2f axis-angles:%6.2f,%6.2f start %6.2f end %6.2f\n'\
			%(str(self.origin),self.ratio,raddeg*self.angle0,raddeg*self.angle1,\
				raddeg*(self.start_E+self.angle0),raddeg*(self.end_E+self.angle0))+\
			'\tbegin: ' +str(self.points[0]) + ' end' + str(self.points[-1])
		return s

	def __str__(self):
		s='Ellip: ' +str(self.points[0]) + '->' + str(self.points[-1])
			
		return s
		
	def reverse(self):
		self.points=(self.points[-1],self.points[0])
		if self.reversemarker==0:
			self.reversemarker=1
		else:
			self.reversemarker=0

	def getNPoints(self,nf):
		"""retuns N equidistanced points from start to end on the object
		"""
		n=int(round(nf))
				
		if 	self.end_E>self.start_E: #ok
			bow_r=self.end_E-self.start_E		#RADIANTS
		else:#bow passes over zero
			bow_r=(2*math.pi-self.start_E)+self.end_E
		step=bow_r/n
		li=[]
		
		if self.reversemarker==0:
			for i in range(n):
				g=self.calcPoint(i*step+self.start_E)
				#message( g)
				li.append(g)
		else:
			for i in range(n):
				g=self.calcPoint(self.end_E-i*step)
				#message( g)
				li.append(g)
		

		return li

class Spline(ExtendedPathObject):
	def __init__(self,
			points=[],knotvalues=[],
			knots=0,
			degree=0,
			controlpoints=0,
			fitpoints=0,
			color=0,
			layer=0):
		ExtendedPathObject.__init__(self)
		#values from the dxf-file
		self.points=points
		self.knotvalues=knotvalues
		self.color=color
		self.layer=layer
		self.knots=knots
		self.degree=degree
		self.controlpoints=controlpoints
		self.fitpoints=fitpoints

		message( self.strall())
		message( str(self))
		
	def getLength(self):
		if debug>0:
			message( 'wrong length')
		return 1
		
	def strall(self):
		s='\t\tSpline: %i degree %i\n\t\t Points %s\n'\
			%(self.knots,self.degree,str(self.points))+\
			'\t\tbegin: ' +str(self.points[0]) + ' end' + str(self.points[-1])\
			
			
		return s

	def __str__(self):
		s='Spline: ' +str(self.points[0]) + '->' + str(self.points[-1])
		return s
					
			
class LWPolyline(MultiPathObject):
	"""
	is separated into lines internally
	"""
	def __init__(self,
			points=[],
			vertices=0,
			color=0,
			layer=0):
		ExtendedPathObject.__init__(self)
		#values from the dxf-file
		self.points=points
		self.vertices=vertices
		self.color=color
		self.layer=layer

		self.objects=self.getAsLines()
		
		#message( self.strall())
		#message( str(self))
		
	def getLength(self):
		if debug>0:
			message( 'wrong length')
		return 1
		
	def strall(self):
		s='\t\tLWPolyline: %i vertices\n'\
			%(self.vertices)+\
			'\t\tbegin: ' +str(self.points[0]) + ' end' + str(self.points[-1])
	#	s='\t\tLWPolyline: %i vertices \n\t\t Points %s\n'\
	#		%(self.vertices,str(self.points))+\
	#		'\t\tbegin: ' +str(self.points[0]) + ' end' + str(self.points[-1])
		return s

	def __str__(self):
		s='LWPolyline: ' +str(self.points[0]) + '->' + str(self.points[-1])
		return s
		
	def getAsLines(self):
		l=[]
		for k in range(len(self.points)-1):
			#not that nice

			l.append( Line(self.points[k].x, self.points[k].y, self.points[k].z, 
				self.points[k+1].x, self.points[k+1].y, self.points[k+1].z ) )
		return l
		
	def getObjects(self):
		return self.objects
			
			
####################################################			

def parse_entity_Line(entity):
	for group in entity:
		code=group[0]
		value=group[1]
		color=None
		
		if code==dxfc['name']:
			pass;
		elif code==dxfc['x0']:
			x0=value
		elif code==dxfc['y0']:
			y0=value
		elif code==dxfc['z0']:
			z0=value
		elif code==dxfc['x1']:
			x1=value
		elif code==dxfc['y1']:
			y1=value
		elif code==dxfc['z1']:
			z1=value
		elif code==dxfc['color']:
			color=value
		elif code==dxfc['layer']:
			layer=value
	#needed:
	#interpol. foumula
	if not color:
		if debug>0: message( 'No color set/found assuming 1 as color')
		color=1
	
		
	obj=Line(x0=x0,y0=y0,z0=z0,x1=x1,y1=y1,z1=z1,color=color,layer=layer)
	if debug>0:
		message( 'parsed: '+ str(obj) + ' color:' +str(color))
	return obj

def parse_entity_Point(entity):
	color=None
	for group in entity:
		code=group[0]
		value=group[1]
		
		if code==dxfc['name']:
			pass;
		elif code==dxfc['x0']:
			x0=value
		elif code==dxfc['y0']:
			y0=value
		elif code==dxfc['z0']:
			z0=value
		elif code==dxfc['color']:
			color=value
		elif code==dxfc['layer']:
			layer=value
	#needed:
	#interpol. foumula
	if not color:
		if debug>0: message( 'No color set/found assuming 1 as color')
		color=1
	obj=Point(x=x0,y=y0,z=z0,color=color,layer=layer)
	if debug>0:
		message( 'parsed: '+ str(obj) + ' color:' +str(color))
	return obj

def parse_entity_Arc(entity):
	
	color=None
	for group in entity:
		code=group[0]
		value=group[1]
		#message( type(value))
		
		if code==dxfc['name']:
			pass;
		elif code==dxfc['x0']:
			x0=value
		elif code==dxfc['y0']:
			y0=value
		elif code==dxfc['z0']:
			z0=value
		elif code==dxfc['angle0']:
			angle0=value
		elif code==dxfc['radius0']:
			radius=value
		elif code==dxfc['angle1']:
			angle1=value
		elif code==dxfc['color']:
			color=value
		elif code==dxfc['layer']:
			layer=value
	
	#needed:
	#start point
	#end point
	#length
	#interpol. foumula
	if not color:
		if debug>0: message( 'No color set/found assuming 1 as color')
		color=1

	obj=Arc(x0=x0,y0=y0,z0=z0,radius=radius,angle0=angle0,angle1=angle1,
			color=color,layer=layer)
	if debug>0:
			message( 'parsed: '+ str(obj) )
	return obj

def parse_entity_Ellipse(entity):
	
	color=None
	for group in entity:
		code=group[0]
		value=group[1]
		#message( type(value))
		
		if code==dxfc['name']:
			pass;
		elif code==dxfc['x0']:
			xcenter=value
		elif code==dxfc['y0']:
			ycenter=value
		elif code==dxfc['z0']:
			zcenter=value
		elif code==dxfc['x1']:
			end_rel_major_x=value
		elif code==dxfc['y1']:
			end_rel_major_y=value
		elif code==dxfc['z1']:
			end_rel_major_z=value
		elif code==dxfc['ratio']:
			ratio=value
		elif code==dxfc['start']:
			start_parameter=value
		elif code==dxfc['end']:
			end_parameter=value
		elif code==dxfc['color']:
			color=value
		elif code==dxfc['layer']:
			layer=value
	#needed:
	#start point
	#end point
	#length
	#interpol. foumula

	if not color:
		if debug>0: message( 'No color set/found assuming 1 as color')
		color=1

	obj=Ellipse(xcenter=xcenter,ycenter=ycenter,zcenter=zcenter,
		end_rel_major_x=end_rel_major_x,
		end_rel_major_y=end_rel_major_y,
		end_rel_major_z=end_rel_major_z,
		ratio=ratio,start_parameter=start_parameter,end_parameter=end_parameter,
		color=color,layer=layer)
	if debug>0:
			message( 'parsed: '+ str(obj))
	return obj

def parse_entity_Spline(entity):
	message( 'parsing Spline')
	#objectlists!!!
	points=[]
	knotvalues=[]
	color=None
	for group in entity:
		code=group[0]
		value=group[1]
		#message( '\t\tcode %s value %s'%( str(group),str(value)))
		#multiple contents
		
		if code==dxfc['name']:
			pass;
		elif code==dxfc['x0']:
			xval=value
		elif code==dxfc['y0']:
			yval=value
		elif code==dxfc['z0']:
			zval=value
			points.append((xval,yval,zval))
			
		elif code==dxfc['knotvalue']:
			knotvalues.append(value)
		elif code==dxfc['splineflag']:
			if value!=8:
				message( 'Warning no planar Spline')
		elif code==dxfc['knots']:
			knots=value
		elif code==dxfc['degree']:
			degree=value
		elif code==dxfc['controlpoints']:
			controlpoints=value
		elif code==dxfc['fitpoints']:
			fitpoints=value
		elif code==dxfc['color']:
			color=value
		elif code==dxfc['layer']:
			layer=value
		else:
			#message( '\t\t\tunparsed code %s value %s'%( str(group),str(value)))
			pass
	
	#message( 'points: '+str(points))
	if not color:
		if debug>0: message( 'No color set/found assuming 1 as color')
		color=1
	
	obj=Spline(
		points=points,
		knotvalues=knotvalues,
		color=color,layer=layer,
		knots=knots,
		degree=degree,
		controlpoints=controlpoints,
		fitpoints=fitpoints
		)
	obj=None
	return obj
	
def parse_entity_Lwpolyline(entity):
	#message( 'parsing LWPolyline')
	#objectlists!!!
	points=[]
	knotvalues=[]
	color=None
	for group in entity:
		code=group[0]
		value=group[1]
		#message( '\t\tcode %s value %s'%( str(group),str(value)))
		#multiple contents
		
		if code==dxfc['name']:
			pass;
		elif code==dxfc['x0']:
			xval=value
		elif code==dxfc['y0']:
			yval=value
			#only x and y coordinates occour
			points.append(Point(x=xval,y=yval))
			
		#elif code==dxfc['z0']:
		#	zval=value
		#	points.append((xval,yval,zval))
			
		elif code==dxfc['vertices']:
			vertices=value
		
		elif code==dxfc['color']:
			color=value
		elif code==dxfc['layer']:
			layer=value
		else:
			#message( '\t\t\tunparsed code %s value %s'%( str(group),str(value)))
			pass
	
	#message( 'points: '+str(points))
	if not color:
		if debug>0: message( 'No color set/found assuming 1 as color')
		color=1

	obj=LWPolyline(
		points=points,
		vertices=vertices,
		color=color,layer=layer
		)

	return obj
			
