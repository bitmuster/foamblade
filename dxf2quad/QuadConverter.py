
############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

import math
from MessageWrapper import *
#from PathObjects import *
import PathObjects

debug=0

def ask_user_for_pps():
	debug=0
	pps_default=100
	useful=False
	while not useful:
		message( '\nHow many points shall be used for a subpath ( The space between two synchronizers)?')
		message( 'default is 100 (press Enter for default)')
		s=input()
		if	s=='':
			if debug>0:message( 'using default')
			useful=True
			pps=pps_default
		else:
			try:
				int(s)	
				pps=int(s)
				#if debug>0:
				message('using %i as pps'%pps)
				useful=True
			except:
				pass
	return pps

	
def path_to_quad_length (path,ppmm):
	"""Uses the path length to sychronize
	This produces only a Quad not a FoamBlade Quad
	"""
	message( '\nConverting Path to Quad')
	#message('Using %i Points per Subgraph'%pps)
	message('Using %i Points per millimeter'%ppmm)

	precision=9 # used in round statements

	if debug>0:
		message( '-'*84)
	quad=[]
	for subpath in path:
		amount_xy=len(subpath[0] )
		amount_uv=len(subpath[1] )
		length_xy=subpath[2]
		length_uv=subpath[3]
		maxlength= max(length_xy, length_uv)
		
		pointsInSubgraph= int(round(maxlength * ppmm))
		
		if debug>0:
			message( 'length xy: %6f with %i objects' %(length_xy,amount_xy))
			message( 'length uv: %6f with %i objects' %(length_uv,amount_uv))
			message( 'maximum length is %6f leads to %6f points in this subgraph ' %(maxlength,pointsInSubgraph))
		xypoints=0
		xypoints_left=0	#a fractional part <1 may be left
		lost_objects_xy=0	#lost because it is too small for the current precision
		quadlistxy=[]
		line_optimizer=False
		if ( len(subpath[0])==1  and len(subpath[1])==1):
			if isinstance(subpath[0][0], PathObjects.Line) and isinstance(subpath[1][0], PathObjects.Line):
				print("Optimizing lines path")
				line_optimizer=True


		for xyobj in subpath[0]:
			l=xyobj.getLength()
			points=l/length_xy*pointsInSubgraph
			#points=l/length_xy*pps
			xypoints+=points
			xypoints_left+=points
			if round(xypoints_left,precision)>=1: #due to rounding errors in the last one
				left_temp=xypoints_left
				if line_optimizer:
				    poo=1
				else:
				    poo=math.floor(round(xypoints_left,precision))

				xypoints_left-=poo
				#print 'requesting %i points leaving %f of %f'%(poo,xypoints_left,left_temp)
				quadlistxy+=xyobj.getNPoints(poo)
			else:
				lost_objects_xy+=1
				#print 'leaving %f points'%(xypoints_left)

		    
		uvpoints=0
		uvpoints_left=0	#a fractional part <1 may be left
		lost_objects_uv=0	#lost because it is too small for the current precision
		quadlistuv=[]

		for uvobj in subpath[1]:
			l=uvobj.getLength()
			points=l/length_uv*pointsInSubgraph
			#points=l/length_uv*pps
			uvpoints+=points
			uvpoints_left+=points
			if round(uvpoints_left,precision)>=1: #due to rounding errors in the last one
				left_temp=uvpoints_left
				if line_optimizer:
				    poo=1
				else:
				    poo=math.floor(round(uvpoints_left,precision))
				
				uvpoints_left-=poo
				#print 'requesting %i points leaving %f of %f'%(poo,xypoints_left,left_temp)
				quadlistuv+=uvobj.getNPoints(poo)
			else:
				lost_objects_uv+=1
				#print 'leaving %f points'%(xypoints_left)
					


		#if round(xypoints,precision)!=pps or round(xypoints,precision)!=pps:
		if round(xypoints,precision)!=pointsInSubgraph or round(uvpoints,precision)!=pointsInSubgraph:
			message( 'Conversation ERROR! Got points xy:%f, uv: %f'%(xypoints,uvpoints))
		
		#if not(len(quadlistxy)==len(quadlistuv)==pps):
		if not(len(quadlistxy)==len(quadlistuv)==pointsInSubgraph) and not line_optimizer :
			message( 'Conversation ERROR! Got point amount xy:%f, uv: %f'%(len(quadlistxy),len(quadlistuv)))
		if line_optimizer and not (len(quadlistxy)==len(quadlistuv)==1 ):
		    message( 'Conversation ERROR! Got error while optimization')
		    
		if debug>0:message( 'overall xy:%i uv:%i points; lost objects: xy:%i uv:%i'%(len(quadlistxy),len(quadlistuv),lost_objects_xy,lost_objects_uv))
		
		for i in range(len (quadlistxy)):
			quad.append((	(quadlistxy[i]).x,(quadlistxy[i]).y,(quadlistuv[i]).x,(quadlistuv[i]).y))
		
		if debug>0:message( '-'*84)

	# add the last point of the last subpath
	print((subpath[0][-1]).getEndPoints())
	quad.append ( (
		((subpath[0][-1]).getEndPoints())[1].x,  ((subpath[0][-1]).getEndPoints())[1].y, 
		((subpath[1][-1]).getEndPoints())[1].x,  ((subpath[1][-1]).getEndPoints())[1].y ))
	
		
		
		
	
	message( 'Got %i synchronized lines in Quad'%(len(quad)))
	return quad

	
	
##def ask_user_for_ppo():
##	ppo_default=100
##	useful=False
##	while not useful:
##		print '\nHow many points shall be used for objects \n(on the side with the longer path) ?\n(This controls the precision of the quad generation.)'
##		print 'default is 100 (press Enter for default)'
##		s=raw_input()
##		if	s=='':
##			if debug>0:print 'using default'
##			useful=True
##			ppo=ppo_default
##		else:
##			try:
##				int(s)	
##				ppo=int(s)
##				if debug>0:print 'using %i as ppo'%ppo
##				useful=True
##			except:
##				pass
##	return ppo
	
##def path_to_quad_cppo (path):
##	"""up to date only a custom hack synchronisation will not be correct
##	sometimes it is buggy
##	cppo constant points per object
##	"""
##	ppo=ask_user_for_ppo()
##	print '\nTrying to convert Path to Quad'
##	print '\tTODO: synchronisation will still not be absolute precise if you use ellipses'
##	if debug>0:
##		print '-'*84
##
##	#ppo=10#50#20
##	print '...using %i points per Object'%ppo
##
##	quad=[]
##	for subpath in path:
##		#points on complete subpath
##		pocs=ppo*max( len(subpath[0]),len(subpath[1]))
##		#points per Object on shorter path
##		#is float since a point may go lost due to rounding errors if integers are used
##		#pposhort is the rest that would go missing in case of rounding errors
##		#the correction is done slightly adding pposhort_rest to the points per object
##		if 0!=min( len(subpath[0]),len(subpath[1])):
##			pposhort_float=float(pocs)/min( len(subpath[0]),len(subpath[1]))
##			pposhort_int=int(round(float(pocs)/min( len(subpath[0]),len(subpath[1]))))
##			pposhort_rest=pposhort_float-pposhort_int
##		else:
##			pposhort_float=0
##			pposhort_int=0
##			pposhort_rest=0
##			
##		if pposhort_rest:
##			if debug>0:print "pposhort_rest %f"%(pposhort_rest)
##		
##		if debug>0:print 'using %i points on subpath with %f per object on the shorter side'%(\
##			pocs,pposhort_float)
##		
##		if debug>0:print 'Sub-Path Length: xy:%8.4f uv:%8.4f'%(subpath[2],subpath[3])
##		xypath=[]
##		uvpath=[]
##		pposhort_akk=0
##		if len(subpath[0]) >= len(subpath[1]):
##			for pObj in subpath[0]:
##				xypath+=pObj.getNPoints(ppo)
##			for pObj in subpath[1]:
##				#usually the pposhort is a fraction
##				#...
##				pposhort_akk+=pposhort_rest
##				if 0<=pposhort_akk<1:
##					uvpath+=pObj.getNPoints(pposhort_int)
##				elif pposhort_akk>1:
##					uvpath+=pObj.getNPoints(pposhort_int+1)
##					pposhort_akk-=1
##				#print pposhort_akk
##				
##				
##		else:
##			for pObj in subpath[0]:
##				pposhort_akk+=pposhort_rest
##				if 0<=pposhort_akk<1:
##					xypath+=pObj.getNPoints(pposhort_int)
##				elif pposhort_akk>1:
##					xypath+=pObj.getNPoints(pposhort_int+1)
##					pposhort_akk-=1
##				#print pposhort_akk
##			for pObj in subpath[1]:
##				uvpath+=pObj.getNPoints(ppo)
##				
##		if debug>0:print 'got %i xy-points and %i uv-points on subpath'%(len(xypath),len(uvpath))
##		if len(xypath)!=len(uvpath):
##			raise PathError( 'subpath lengthes are not the same')
##		
##		for i in range(len(xypath)):
##			quad.append( ( xypath[i].x, xypath[i].y,uvpath[i].x,uvpath[i].y) )
##			
##		if debug>0:print '-'*84
##	
##	print 'Got %i synchronized lines in Quad'%(len(quad))
##	return quad
