
set -x

rm -f *.quad

for i in *dxf; do python3 ../dxf2quad.py $i $i.quad  > /dev/null ; done

cd generated_v0.5

for i in *.quad; do diff -u $i ../$i ; done



# python3 dxf2quad.py paths/allObjects.dxf paths/allObjects.quad
# python ../FoamBladeViewer/fbv.py paths/allObjects.quad
