
Test data to make sure we do not break functionality during
a code update.

All example paths converted with:

    for i in tests/*; do python dxf2quad.py $i $i.quad ; done

    for i in tests*.quad; do python ../../FoamBladeViewer/fbv.py $i; done

