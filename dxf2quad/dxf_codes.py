
############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

#group codes of the dxf format

#eventually split the code table since lots of numbers habe different meaning according to the context
#->search n numbers depending on the context

dxfc={	#names are the keys
	'name':0,
	'layer':8,
	
	'x0':10,	'x1':11,	'x2':12,	'x3':13, #8 are possible
	'y0':20,	'y1':21,	'y2':22,	'y3':23,
	'z0':30,	'z1':31,	'z2':32,	'z3':33,
	'radius0':40,
	'ratio':40,
	'start':41,
	'end':42,
	'angle0':50,    'angle1':51,    'angle2':52,    'a3':53,
	'color':62,
	
	#spline
	#multiple x0,y0,z0
	'knotvalue':40, #multiple in spline
	'splineflag':70,
	'degree':71,
	'knots':72,
	'controlpoints':73,
	'fitpoints':74,
	
	#lwpolyline
	'vertices':90,
	
	#layers
	'layername':2,
	'layerflags':70,
	}
	
#angles
#0 right
#90 North
#270 South

#ellipses
#define the begin and end in clockwise direction

ACADVER={
	'AC1018':'2004, 2005, 2006',
	'AC1015':'2002, 2000i, 2000',
	'AC1014':'14',
	'AC1012':'13',
	'AC1009':'12, 11',
	'AC1006':'10',
	'AC1004':'9',
	'AC1002':'2',
	}

