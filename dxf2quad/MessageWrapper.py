
############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

#wraps the standard print option to another operation

#printoperation=None

##def printoperation(msg):
##	print 'print :\t' +str(msg)

##class MsgWrapper(object):
##	"""Fully static class"""
##	def __init__(self,wrapper):
##		if wrapper:
##			self.msg=wrapper
##		pass
##	
##	#smethod=None #class variable (funttion)
##	#to be overitten later
##	def smethod(m):
##		pass
##		
##	def msg(m):
##		#print MsgWrapper.smethod
##		#print 'print :\t' +str(m)
##		MsgWrapper.smethod(m)
##		
##	msg=staticmethod(msg)
##	smethod=staticmethod(smethod)
	
	
##def message(msg):
##	if MsgWrapper.smethod:
##		MsgWrapper.msg(msg)
##	else:
##		print msg
##		

#example to be called in another file

#import MessageWrapper		#both!!!
#from MessageWrapper import *
#
#MessageWrapper.msgfunc=lambda arg:	textfield.insert(END,arg)

msgfunc=None

def message(msg):
	if msgfunc:
		msgfunc(msg+ '\n')
	else:
		print(msg)

