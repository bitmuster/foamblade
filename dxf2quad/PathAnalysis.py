
############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

from PathObjects import *
from Errors import *

from MessageWrapper import *

debug=2;

color_uv=5      #blue
color_xy=1 #red
color_sync=2 #yellow
color_begin=3 #green
color_both=6 #magenta

###########
dxf_tables=None
dxf_entities=None
###########
pathes_xy=[]
pathes_uv=[]
sync_lines=[]
start_marker=[]
###########

last_type_choosen_from_user=0   #is only used in ask_for_type

#ignore_other_layers=False # may be set during interaction to True
ignore_other_layers=True # may be set during interaction to True

def ask_user_for_type(obj):
        useful=False
        global last_type_choosen_from_user

        while not useful:
                message( '\nNo matching type found for obj: '+str(obj))
                message( 'It belongs to Layer: %s and has color no. %i'%(obj.layer,obj.color))
                message( """please tell me to wich category it belongs
        1       for xy
        2       for uv
        3       for xy and uv (both)
        4       for an synchonisation Object
        5       for the start-marker
        0       to ignore this object
        i       to ignore every objects on other layers
        q       for exit
        Enter repeats the last option (%s)
        """ %(str(last_type_choosen_from_user)) )
                s=input()
                if s=='q':
                        message ('exiting...')
                        sys.exit(-1)
                elif    s=='':
                        message( 'reusing')
                        r=last_type_choosen_from_user
                        useful=True
                elif s=='i':
                        return -1
                else:
                        try:
                                int(s)
                                r=int(s)
                                useful=True
                                last_type_choosen_from_user=r
                        except:
                                pass
        return r


def analyzePath():
        #orders the object lists
        #
        #produces a list with ordered sublists for
        #the pathes between the sync lines
        # [ [  [..xy.. object list.],[...uv.object list..],[xylen,uvlen] ],.... ]

        message( '\nAnalyzing Path')

        if len ( start_marker) >1:
                raise PathError( 'Too much start markers')
        if len ( start_marker) ==0:
                raise PathError('No start marker found')

        if debug>0:
                message( '*-------------------------------*'    )
                message( '*  Initialisation'    )
                message( '*-------------------------------*'    )

        temp_pathes_xy=pathes_xy[:] # lists of path objects
        temp_pathes_uv=pathes_uv[:]
        temp_sync_lines=sync_lines[:]

        if debug>0:
                message( 'Start: '+ str(start_marker[0]))
                message( 'Start marker has type: '+ str(type(start_marker[0])))
        start=start_marker[0]
        #if not isinstance(start,Line):
        #       raise PathError('Start marker is not a line')

        pos_xy= start.getEndPoints()[0]#actual pos maybe xy
        pos_uv= start.getEndPoints()[1]#actual pos maybe uv

        #get the start pathes

        nextobjlistxy=getConnections(start.getEndPoints()[0], temp_pathes_xy)
        if debug>0:
                message( 'nextobjlistxy: ' + str(nextobjlistxy))
        xy_start=0
        if len (nextobjlistxy)==0:
                nextobjlistxy=getConnections(start.getEndPoints()[1], temp_pathes_xy)
                xy_start=1

        nextobjlistuv=getConnections(start.getEndPoints()[0], temp_pathes_uv)
        uv_start=0
        if len (nextobjlistuv)==0:
                nextobjlistuv=getConnections(start.getEndPoints()[1], temp_pathes_uv)
                uv_start=1

        if debug>0:
                message( "xy starts at %s (%i),uv starts at %s (%i) "%
                        (start.getEndPoints()[xy_start],xy_start,start.getEndPoints()[uv_start],uv_start))
                message( "xy :" + str(nextobjlistxy))
                message( "uv :" + str(nextobjlistuv))

        if len(nextobjlistxy)>1 and (nextobjlistuv)>1  :
                raise PathError('More than one connection from start point on both sides: '+
                        str(nextobjlistxy))

        if len(nextobjlistxy)==0 and len(nextobjlistuv)==0:
                raise PathError('Start point is not connected on both sides')

        if len(nextobjlistxy)>1:
                raise PathError('More than one connection from start point on xy side: '+
                        str(nextobjlistxy))
        elif len(nextobjlistxy)==0:
                raise PathError('Start point is not connected on xy side')

        if len(nextobjlistuv)>1:
                raise PathError('More than one connection from start point on uv side: '+
                        str(nextobjlistuv))
        elif len(nextobjlistuv)==0:
                raise PathError('Start point is not connected on uv side')

        nextobjxy=nextobjlistxy[0]
        posxy=start.getEndPoints()[xy_start] #point


        nextobjuv=nextobjlistuv[0]
        posuv=start.getEndPoints()[uv_start] #point

        if debug>0:
                message('start position xy'+ str(posxy))
                message( 'start position uv'+ str(posuv))

        object_path=[]
        xy_object_list=[]
        uv_object_list=[]
        sub_path_length_xy=0
        sub_path_length_uv=0
        path_length_xy=0
        path_length_uv=0

        iter=0
        while(temp_pathes_xy and temp_pathes_uv and temp_sync_lines):
                if debug>0:
                        message( '*-------------------------------*'    )
                        message( '*  Sub-Sequence '+ str(iter))
                        message( '*-------------------------------*'    )
                iter +=1

                sync_line_xy_found=False
                while(not sync_line_xy_found):

                        #parse till next sync line in xy
                        #print 'looking for synclines in xy'

                        syncxy=getConnections(posxy,temp_sync_lines)
                        if syncxy:
                                #printPath( syncxy)
                                if debug>0:message('xy sync line found: '+ str(syncxy[0]))
                                sync_line_xy_found=True
                        else:
                                nextobjlistxy=getConnections(posxy, temp_pathes_xy)
                                if len(nextobjlistxy)>1:
                                        for i in nextobjlistxy:
                                                message( str(i))
                                        raise PathError('error more than one connection from point on (xy): '+
                                                                str(posxy))
                                elif len(nextobjlistxy)==0:
                                        raise PathError( 'unconnected path at ' + str(posxy))

                                nextobjxy=nextobjlistxy[0]
                                if debug>3:
                                        message( 'found in xy: ' + str(nextobjxy))
                                xy_object_list.append( nextobjxy.setBegin(posxy) )
                                sub_path_length_xy+=nextobjxy.getLength()
                                path_length_xy+=nextobjxy.getLength()

                                posxy=nextobjxy.getOtherEnd(posxy)
                                #print 'continuing on xy side with ' +str(posxy)
                                temp_pathes_xy.remove(nextobjxy)
                sync_line_uv_found=False
                while(not sync_line_uv_found):
                        #parse till next sync line in uv
                        #print 'searching in uv'
                        syncuv=getConnections(posuv,temp_sync_lines)
                        if syncuv:
                                #printPath( syncuv)
                                if debug>0:message( 'uv sync line found: '+ str(syncuv[0]))
                                sync_line_uv_found=True
                        else:
                                nextobjlistuv=getConnections(posuv, temp_pathes_uv)
                                if len(nextobjlistuv)>1:
                                        for i in nextobjlistuv:
                                                message( str(i))
                                        raise PathError('error more than one connection from point on (uv): '+
                                                        str(posuv))
                                elif len(nextobjlistuv)==0:
                                        raise PathError( 'unconnected path at ' + str(posuv))

                                nextobjuv=nextobjlistuv[0]
                                if debug>3:
                                        message( 'found in uv: ' + str(nextobjuv))
                                #print 'test: ' +str(nextobjuv.setBegin(posuv))
                                uv_object_list.append( nextobjuv.setBegin(posuv) )
                                #print uv_object_list
                                sub_path_length_uv+=nextobjuv.getLength()
                                path_length_uv+=nextobjuv.getLength()

                                posuv=nextobjuv.getOtherEnd(posuv)
                                #print 'continuing on uv side with ' +str(posuv)
                                temp_pathes_uv.remove(nextobjuv)
                if debug>0:
                        message( 'position xy'+ str(posxy))
                        message( 'position uv'+ str(posuv))

                if syncxy[0]==syncuv[0]:
                        if debug>0:
                                message('Stepping over Synchronizer')
                                message('subpath-length :'+str( sub_path_length_xy)+' '+str(sub_path_length_uv))

                        temp_sync_lines.remove(syncxy[0])
                        if  xy_object_list or uv_object_list :
                                object_path.append ( [ xy_object_list,uv_object_list,sub_path_length_xy,sub_path_length_uv] )
                        else:
                                if debug>0:
                                        message( 'Nothing to add to object_list')
                        xy_object_list=[]
                        uv_object_list=[]
                        sub_path_length_xy=0
                        sub_path_length_uv=0
                else:
                        raise PathError('sychronizer objects don\'t match. ' +
                        ' Are they propperly connected? : ' + str(syncxy[0])+' and '+str(syncuv[0]))

                if debug>0:
                        message( 'Still unmatched:')
                        message( str(len(temp_pathes_xy )) + ' xy')
                        message( str(len(temp_pathes_uv )) + ' uv')
                        message( str(len(temp_sync_lines )) + ' sync lines'     )
                        #printPath(temp_sync_lines)
                        #print object_path
        #print object_path
        message( 'Path-length :xy:'+str( path_length_xy)+' uv:'+str(path_length_uv))
        message( 'Path analysis successfull')
        message( 'Still unmatched objects: '+ str(len(temp_pathes_xy )) + ' xy '+ str(len(temp_pathes_uv )) + ' uv '+ str(len(temp_sync_lines )) + ' sync lines'        )

        return object_path

def printObjectPath(path):
        #printPath( path)
        message( 'This is the path:')
        message( '*'*84)
        for subpath in path:
                message( 'Sub-Path Length: xy:%8.4f uv:%8.4f'%(subpath[2],subpath[3]))
                for i in range(max( len(subpath[0]),len(subpath[1]))):
                        if len( subpath[0]) > i:
                                s0=str(subpath[0][i])
                        else:
                                s0='\t'*4
                        if len( subpath[1]) > i:
                                s1=str(subpath[1][i])
                        else:
                                s1='' #'\t'*3
                        message( s0 + '\t'*2+s1)
                message( '*'*84)

def printPath(p):
        message( 'Path:')
        message( '*----')
        for  i in p:
                message( '* '+str( i))
        message( '*----')


def parse_entities(entities):

        #At current only the    entities Section is from interrest for us
        message('Analyzing the dxf entities section')
        #entities is a dictionary that contains entities (key is a number)
        #the entities are lists that contain touples these touples
        #constist of grup code + value


        for entity in list(entities.keys()): # for all objects in file
                #print entity
                #print entities[entity]         #list of touples that contain (autocad group code, value)
                group=entities[entity][0]
                #print group            #(10, 40.0)
                if group[0]==0 and group[1].upper()=='LINE':
                        obj=parse_entity_Line(entities[entity])
                elif group[0]==0 and group[1].upper()=='ARC':
                        obj=parse_entity_Arc(entities[entity])
                elif group[0]==0 and group[1].upper()=='ELLIPSE':
                        #print 'found ELLIPSE'
                        obj=parse_entity_Ellipse(entities[entity])

                elif group[0]==0 and group[1].upper()=='SPLINE':
                        message('\t\tnot parsing ' + str(group))
                        obj=parse_entity_Spline( entities[entity])
                        #printEntity(entities[entity])
                        continue
                elif group[0]==0 and group[1].upper()=='POINT':
                        #print '\t\tnot parsing ' + str(group)
                        #print entities[entity]
                        obj=parse_entity_Point(entities[entity])
                        if debug>0:message( 'unuseable: Point %s layer %s'% (str(obj), obj.layer))
                        continue
                elif group[0]==0 and group[1].upper()=='LWPOLYLINE':
                        obj=parse_entity_Lwpolyline(entities[entity])

                elif group[0]==0 and group[1].upper()=='POLYLINE':
                        message('\t\tnot parsing ' + str(group))
                        #obj=parse_entity_Spline(entities[entity])
                        continue
                elif group[0]==0 and group[1].upper()=='MLINE':
                        message( '\t\tnot parsing ' + str(group))
                        #obj=parse_entity_Spline(entities[entity])
                        continue
                elif group[0]==0 and group[1].upper()=='XLINE':
                        message('\t\tnot parsing ' + str(group))
                        #obj=parse_entity_Spline(entities[entity])
                        continue
                else:
                        message( 'NOT parsing ' +group[1])
                        continue

                #sort according to layer
                global ignore_other_layers


                if obj.layer.lower()=='start':
                        start_marker.append(obj)
                        if debug>0:message( 'Start: '+ str(start_marker))
                        sync_lines.append(obj)

                elif obj.layer.lower()=='sync' or obj.layer[0:5].lower()=='sync_':
                        sync_lines.append(obj)

                elif obj.layer.lower()=='xy' or obj.layer[0:3].lower()=='xy_':
                        pathes_xy.append(obj)

                elif obj.layer.lower()=='uv' or obj.layer[0:3].lower()=='uv_':
                        pathes_uv.append(obj)

                elif obj.layer.lower()=='red':
                        if debug>0: message( 'waring: using layer name red to sort')
                        pathes_xy.append(obj)
                elif obj.layer=='blue':
                        if debug>0: message('waring: using layer name blue to sort')
                        pathes_uv.append(obj)

                elif obj.layer.lower()=='both' or obj.layer.lower()=='xyuv' \
                                or obj.layer[0:5].lower()=='xyuv_' or obj.layer[0:5].lower()=='both_':
                        pathes_xy.append(obj)
                        pathes_uv.append(obj)

                elif not ignore_other_layers:
                        r=ask_user_for_type(obj)
                        if r==0:
                                pass #ignore
                        if r==1:
                                pathes_xy.append(obj)
                        elif r==2:
                                pathes_uv.append(obj)
                        elif r==3:
                                pathes_xy.append(obj)
                                pathes_uv.append(obj)
                        elif r==4:
                                sync_lines.append(obj)
                        elif r==5:
                                start_marker.append(obj)
                        elif r==-1:
                                ignore_other_layers=True

        if debug>0:
                printPath(sync_lines)
                message( 'Start makers found: ' + str(len(start_marker)))
        message( str(len(pathes_xy )) + '\txy pathes found')
        message( str(len(pathes_uv )) + ' \tuv pathes found')
        message( str(len(sync_lines )) + '\tsync Objects found')
        message( 'parsed pythoncad output successfully')

def getConnections(point,path):
        #returns a alist of objects on path that have connections to point
        #print 'looking for connections to '+ str(point)
        c=[]
        for object in path:
                for ppoint in object.getEndPoints(): #up to now only two
        #               print ppoint
                        if ppoint==point:
        #                       print str(object) + '== '  + str(ppoint)
                                if object in c:
                                        message( "found twice: ignored")
                                else:
                                        c.append(object)
        #print 'found ' + str(len(c))+ ' connections'
        return c

def flatternObjects():
        """Converts Multipathobjects on the pathes to normal objects
        """
        for p in (pathes_xy,pathes_uv,sync_lines,start_marker):
                for o in p:
                        if isinstance(o,MultiPathObject):
                                p.extend(o.getObjects())
                                p.remove(o)
