#!/usr/bin/python
# -*- coding: utf-8 -*-

############################################################################
#    Copyright (C) 2007-2008 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

#Todo   Pathes with 1 ppo
# multipoint/line Synchronizers
# *.dxf und *.DXF *.Dxf ...

#Changelog
#New in 0.02
#layers with names like xy_***

from PathObjects import *
from PathAnalysis import *
from QuadConverter import *
from Errors import *
from TkInterface import *
import math
import sys
import os

debuglevel=0 #debuglevel
# 1: display of the sub-pathes
# most other files have their own debug variable!!!


degrad=math.pi/180 #someday: change them to degrees() and radiants()
raddeg=180/math.pi



helptext="""usage: dxf2quad.py <infile.dxf> <outfile.quad>

This program produces quad-column files (Quads for FoamBlade) from
cut-pathes in dxf Files.The dxf files have to be drawn in a special
manner described in the documentation howto-dxf.html.
"""





def writeToFile(filename,quad_list):
        """writes a quad to a file
        """
        f=open(filename,'w+')
        os.chmod(filename,0o777) # octal nubmer -> leading 0
        message( '\nWriting data to "' +filename+'"')

        for i in quad_list:
                s="%f   %f      %f      %f"%(i[0],i[1],i[2],i[3])
                f.write(  s +'\n')
        f.close()
        message( 'wrote data file "' +filename+'" with ' + str (len(quad_list)) +' enteries')

def writeToNCFilexyuv(filename,quad_list):
        """writes a quad to a NC file
        """
        f=open(filename,'w+')
        os.chmod(filename,0o777) # octal nubmer -> leading 0
        message( '\nWriting NC data to "' +filename+'"')
        
        f.write("G21 (Millimeters)\n")
        f.write("F500 (FeedRate)\n")
        f.write("G64 P0.001    (Digital Output Control???)\n")
        
        f.write("G0 X0 Y0 U0 V0\n" )

        f.write("(MSG, Press S)\n")
        f.write("M00 (Pause - > Press S)\n")
        
        for i in quad_list:
                s="G1 X%f Y%f U%f V%f"%(i[0],i[1],i[2],i[3])
                f.write(  s +'\n')
                
        f.write("M2 (Stop)\n")
                
        f.close()
        message( 'wrote data file "' +filename+'" with ' + str (len(quad_list)) +' enteries')

def writeToNCFilexyuw(filename,quad_list):
        """writes a quad to a NC file
        """
        f=open(filename,'w+')
        os.chmod(filename,0o777) # octal nubmer -> leading 0
        message( '\nWriting NC data to "' +filename+'"')
        
        f.write("G21 (Millimeters)\n")
        f.write("F500 (FeedRate)\n")
        f.write("G64 P0.001    (Digital Output Control???)\n")
        
        f.write("G0 X0 Y0 Z0 U0 V0 W0\n" )

        f.write("(MSG, Press S)\n")
        f.write("M00 (Pause - > Press S)\n")
        
        for i in quad_list:
                s="G1 X%f Y%f U%f W%f"%(i[0],i[1],i[2],i[3])
                f.write(  s +'\n')
                
        f.write("M2 (Stop)\n")
                
        f.close()
        message( 'wrote data file "' +filename+'" with ' + str (len(quad_list)) +' enteries')


def dxf2quad(filename,outfile,ppmm,debug):
        #debug=1 #debuglevel

        ##parse dxf (dxf_fb)
        try:
                message( '\nParsing dxf file')
                dxfcontent=parse_dxf(filename)
                #print dxf_header['ACADVER']

                dxf_tables = dxfcontent['tables']
                dxf_entities = dxfcontent['entities']
                dxf_header =dxfcontent['header']
                #print dxfcontent[i]

                message( 'Found AutoCad DXF File in Version: "' +ACADVER[ dxf_header['ACADVER']]+ '"')

                if debug>0:
                        for section in list(dxfcontent.keys()):
                                message('DXF-SECTION ' + section+'\tcontains ' +str( len(list(dxfcontent[section].keys())  ) )+' enteries')

        except PathError as e :
                message( 'Error occoured in dxfparser')
                message( str(e))
                #sys.exit(-1)
                return

##      try:
##              parse_layers()
##      except PathError,e :
##              print 'Error while analyzing layers in the dxf-file'
##              print str(e)
##              sys.exit(-1)

        ##parse entities
        try:
                parse_entities(dxf_entities)
        except PathError as e :
                message( 'Error while analyzing the path in the dxf-file')
                message( str(e))
                #sys.exit(-1)
                return


        ##analyze Path
        ##from here it should be dxf independent

        try:
                flatternObjects()
        except PathError as e :
                message( 'Error while Flattening Entities')
                message( str(e))
                #sys.exit(-1)
                return

        try:
                path=analyzePath()
        except PathError as e :
                message( 'Error while analyzing the path in the dxf-file')
                message( str(e))
                #sys.exit(-1)
                return

        if debug>0:
                printObjectPath(path)


        ##convert to quad and write to file

        try:
                dxfquad=path_to_quad_length(path,ppmm)
        except PathError as e:
                message( 'Error while converting the path to a Quad')
                message( str(e))
                #sys.exit(-1)
                return

        if outfile:
                if len(dxfquad)>=0:
                        writeToFile(outfile,dxfquad)
                        writeToNCFilexyuv(outfile+"xyuv.ngc",dxfquad)
                        #writeToNCFilexyuw(outfile+"xyuw.ngc",dxfquad)
                else:
                        message('Quad is empty, is the quad bulit acoording to the guidelines?')
        else:
                return dxfquad #

write_to_file=False

if __name__=='__main__':
        message( '\nDXF2Quad 0.04 Copyright Michael Abel 2008')
        message( 'Released under the GNU GENERAL PUBLIC LICENSE')

        if len(sys.argv) != 1    :
                if sys.argv[1]=='-h' or sys.argv[1]=='--help':
                        message( helptext)
                        sys.exit(-1)

        if len(sys.argv) != 3    :
                message( "usage: dxf2quad.py <infile.dxf> <outfile.quad>")
                sys.exit(-1)

        filename=sys.argv[1]
        outfile=sys.argv[2]
        #pp=None # the user will be asked
        #pp=100
        ppmm=10
        print('Correct the pp input')

        dxf2quad(filename,outfile,ppmm,debuglevel)

        #message('Press the "any-key" to continue.')
        #raw_input()







