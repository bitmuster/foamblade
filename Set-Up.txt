
Setting up a new development system
===================================
Basis is a 10.04 Live-CD. 

This is surely installing a few packages too much.

Packages:
=========

sudo apt-get install vim git-core synaptic gitk git-gui

#http://wiki.linuxcnc.org/cgi-bin/wiki.pl?Installing_EMC2
#2.7. Building emc2 (with documents)
#ca. 750MB
#sudo apt-get install asciidoc source-highlight dblatex dvipng

sudo apt-get install libpth-dev tcl8.5-dev tk8.5-dev bwidget libxaw7-dev libreadline5-dev python-dev libglu1-mesa-dev libxinerama-dev autoconf python-tk libglib2.0-dev libxft-dev gettext

sudo apt-get install python-gnome2 python-glade2 python-numpy python-imaging  python-xlib python-gtkglext1 python-configobj python-gtksourceview2 

#For FoamBlade
sudo apt-get install python-scientific mayavi2
sudo apt-get install python-scipy python-numpy python-matplotlib

Cloning Repositories:
=====================

git clone git://gitorious.org/foambladesuite/foambladesuite.git foambladesuite
git clone git://git.linuxcnc.org/git/emc2.git emc2-dev

Further Packages
================

cd emc2-dev/

debian/configure -a (maybe "sim" isntead of a)

dpkg-checkbuilddeps
dpkg-checkbuilddeps: Unmet build dependencies: dvipng texlive-extra-utils texlive-latex-recommended texlive-fonts-recommended texlive-lang-french texlive-lang-german texlive-lang-spanish texlive-lang-polish texlive-font-utils libreadline-dev asciidoc (>= 8.5) source-highlight dblatex (>= 0.2.12) groff python-lxml libboost-python-dev texlive-lang-cyrillic

e.g.:

sudo apt-get install dvipng texlive-extra-utils texlive-latex-recommended texlive-fonts-recommended texlive-lang-french texlive-lang-german texlive-lang-spanish texlive-lang-polish texlive-font-utils libreadline-dev asciidoc source-highlight dblatex groff python-lxml libboost-python-dev texlive-lang-cyrillic


Building
========

git clean -x -d -f

cd src

./autogen.sh      
#./configure --enable-simulator
./configure
#make clean
make -j 2 for two cores

sudo make setuid 

#OLD
. ../scripts/emc-environment
emc

#NEW:
. ../scripts/rip-environment
linuxcnc

