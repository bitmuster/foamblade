#!/usr/bin/env python

############################################################################
#    Copyright (C) 2007-2012 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

import pygtk        #gnome support
pygtk.require('2.0')
import gtk
import gtk.glade

import sys
import time

debug=0

#import thread

import PathContainer

class Viewer:

    tree = None #tree for the gtk objects
    count=0

##  def drawStandardProfile(self):
##      """ probably outdated
##      """
##      #print 'printing Standard Profile'
##      self.clearCanvas()
##
##      #self.hscale.set_range(0,self.viewList.getListSize())
##      self.hscale.set_range(0,len(self.viewList)-1)
##
##      k=5
##      self.drawable_window.draw_line(self.gc,self.xoffset+k,self.yoffset+k,self.xoffset-k,self.yoffset-k)
##      self.drawable_window.draw_line(self.gc,self.xoffset-k,self.yoffset+k,self.xoffset+k,self.yoffset-k)
##
##
##      #p=self.viewList.getItem(0)
##      p=self.viewList[0]
##      xkoordold= int(self.scale*p[0])+self.xoffset
##      ykoordold=-int(self.scale*p[1])+self.yoffset
##
##      #for i in range(1,self.viewList.getListSize()-1):
##
##      for i in range(1,len(self.viewList)): #-1
##          #p=self.viewList.getItem(i)
##          p=self.viewList[i]
##          xkoord= int(self.scale*p[0])+self.xoffset
##          ykoord=-int(self.scale*p[1])+self.yoffset
##          self.drawable_window.draw_line(self.gc,xkoord,ykoord,xkoordold,ykoordold)
##          xkoordold=xkoord
##          ykoordold=ykoord
    def drawDoubleColumn(self):
        #Copied from draw Quad !!!!
        #print 'printing DoubleColum'

        self.hscale.set_range(0,len(self.viewList)-1)

        p=self.viewList[0]

        xmax=ymax=0
        xmin=ymin=0
        for i in range(1,len(self.viewList)): #-1
            p=self.viewList[i]
            if xmax<self.scale*p[0]:
                xmax=self.scale*p[0]
            if xmin>self.scale*p[0]:
                xmin=self.scale*p[0]

            if ymax<self.scale*p[1]:
                ymax=self.scale*p[1]
            if ymin>self.scale*p[1]:
                ymin=self.scale*p[1]

        objectx=int(xmax+abs(xmin))
        objecty=int(ymax+abs(ymin))

        #print 'object size in pixel: ' + str(objectx) + '  ' + str(objecty)

        borderwidth=50

        #canvas dimension to request
        width =objectx+2*borderwidth
        height=objecty+2*borderwidth

        #print 'object dimensions width %i height %i' %( width,height)
        self.drawingarea.set_size_request(width,height)

        #time.sleep(1)
        #self.drawable_window.resize(width,height)
        self.clearCanvas()

        s=self.drawable_window.get_size()
        #print 'draw window size ' + str(s)
        #s=self.drawingarea.get_size()
        #print s

        self.xoffset=int( abs(xmin))+borderwidth
        self.yoffset=int( abs(ymax))+borderwidth

        #print str(self.xoffset)+' '+str(self.yoffset)+' '+str(self.uoffset)+' '+str(self.voffset)
        k=5
        self.drawable_window.draw_line(self.xy_gc,self.xoffset+k,self.yoffset+k,self.xoffset-k,self.yoffset-k)
        self.drawable_window.draw_line(self.xy_gc,self.xoffset-k,self.yoffset+k,self.xoffset+k,self.yoffset-k)

        p=self.viewList[0]
        xkoordold= int(self.scale*p[0])+self.xoffset
        ykoordold=-int(self.scale*p[1])+self.yoffset

        for i in range(1,len(self.viewList)): #-1
            #p=self.viewList.getItem(i)
            p=self.viewList[i]
            xkoord= int(self.scale*p[0])+self.xoffset
            ykoord= - int(self.scale*p[1])+self.yoffset
            #if self.xoffset+xmin<xkoord <self.xoffset+xmax:
            #if self.yoffset+ymin<ykoord <self.yoffset+ymax:
            self.drawable_window.draw_line(self.xy_gc,xkoord,ykoord,xkoordold,ykoordold)
            xkoordold=xkoord
            ykoordold=ykoord


    def drawQuadColumn(self):
        # dras Double is copied from this method
        #print 'printing QuadColum'

        #self.hscale.set_range(0,self.viewList.getListSize())
        self.hscale.set_range(0,len(self.viewList)-1)


        #p=self.viewList.getItem(0)
        p=self.viewList[0]

        xmax=ymax=vmax=umax=0
        xmin=ymin=vmin=umin=0
        for i in range(1,len(self.viewList)): #-1
            p=self.viewList[i]

            if xmax<self.scale*p[0]:
                xmax=self.scale*p[0]
            if xmin>self.scale*p[0]:
                xmin=self.scale*p[0]

            if ymax<self.scale*p[1]:
                ymax=self.scale*p[1]
            if ymin>self.scale*p[1]:
                ymin=self.scale*p[1]

            if umax<self.scale*p[2]:
                umax=self.scale*p[2]
            if umin>self.scale*p[2]:
                umin=self.scale*p[2]

            if vmax<self.scale*p[3]:
                vmax=self.scale*p[3]
            if vmin>self.scale*p[3]:
                vmin=self.scale*p[3]

        objectx=int(xmax+abs(xmin))
        objecty=int(ymax+abs(ymin))
        objectu=int(umax+abs(umin))
        objectv=int(vmax+abs(vmin))


        #print 'object size in pixel: ' + str(objectx) + '  ' + str(objecty)

        #uv_offset=50
        uv_offset=0
        borderwidth=50

        #canvas dimension to request
        width =max(objectx,objectu)+uv_offset+2*borderwidth
        height=max(objecty,objectv)+uv_offset+2*borderwidth
        self.win_width=width;
        self.win_height=height;
        #print 'object dimensions width %i height %i' %( width,height)

        self.drawingarea.set_size_request(width,height)
        #self.drawable_window.resize(width,height)
        #print 'resized %i %i'%(width,height)
        #time.sleep(1)
        #self.drawable_window.resize(width,height)      #superstrange it works best without resizing
        self.clearCanvas()

        #s=self.drawable_window.get_size()
        #print 'draw window size ' + str(s)
        #s=self.drawingarea.get_size()
        #print s

        separate_orgins=False
        if separate_orgins:
            self.xoffset=abs(int(xmin))+uv_offset+borderwidth
            self.yoffset=int(ymax)+uv_offset+borderwidth
            self.uoffset=abs(int(umin))+borderwidth
            self.voffset=int(vmax)+ borderwidth
        else:
            self.xoffset=int( abs(min(xmin,umin)))+borderwidth
            self.yoffset=int( abs(max(ymax,vmax)))+borderwidth
            self.uoffset=self.xoffset
            self.voffset=self.yoffset

## Dimension box
##      self.drawable_window.draw_rectangle(self.xy_gc, False,
##          int(xmin)+self.xoffset
##          ,int(ymax)+self.yoffset,
##          int(objectx),int(objecty))
##      self.drawable_window.draw_rectangle(self.uv_gc, False,
##          int(umax)*self.scale+self.uoffset,
##          int(vmax)*self.scale+self.voffset,
##          int(objectu)*self.scale,-int(objectv)*self.scale)

        #print str(self.xoffset)+' '+str(self.yoffset)+' '+str(self.uoffset)+' '+str(self.voffset)
        k=5
        self.drawable_window.draw_line(self.xy_gc,self.xoffset+k,self.yoffset+k,self.xoffset-k,self.yoffset-k)
        self.drawable_window.draw_line(self.xy_gc,self.xoffset-k,self.yoffset+k,self.xoffset+k,self.yoffset-k)

        self.drawable_window.draw_line(self.uv_gc,self.uoffset+k,self.voffset+k,self.uoffset-k,self.voffset-k)
        self.drawable_window.draw_line(self.uv_gc,self.uoffset-k,self.voffset+k,self.uoffset+k,self.voffset-k)


        p=self.viewList[0]
        xkoordold= int(self.scale*p[0])+self.xoffset
        ykoordold=-int(self.scale*p[1])+self.yoffset

        for i in range(1,len(self.viewList)): #-1
            #p=self.viewList.getItem(i)
            p=self.viewList[i]
            xkoord= int(self.scale*p[0])+self.xoffset
            ykoord= - int(self.scale*p[1])+self.yoffset
            #if self.xoffset+xmin<xkoord <self.xoffset+xmax:
            #if self.yoffset+ymin<ykoord <self.yoffset+ymax:
            self.drawable_window.draw_line(self.xy_gc,xkoord,ykoord,xkoordold,ykoordold)
            xkoordold=xkoord
            ykoordold=ykoord

        p=self.viewList[0]
        ukoordold= int(self.scale*p[2])+self.uoffset
        vkoordold=-int(self.scale*p[3])+self.voffset

        for i in range(1,len(self.viewList)): #-1
            #p=self.viewList.getItem(i)
            p=self.viewList[i]
            ukoord= int(self.scale*p[2])+self.uoffset
            vkoord= - int(self.scale*p[3])+self.voffset
            #if umin<ukoord <umax:
            #   if vmin<vkoord <vmax:
            self.drawable_window.draw_line(self.uv_gc,ukoord,vkoord,ukoordold,vkoordold)
            ukoordold=ukoord
            vkoordold=vkoord
        pass



    def analyseTree(self,tree):
        print 'tree'
        for n in  tree.get_widget_prefix(''):
            print n
        if debug:print 'tree end'

    def on_viewer_scrolledwindow_scroll_child(self,*args):
        if debug:print 'scrolled'
    def on_viewer_scrolledwindow_scroll_event(self,*args):
        if debug:print 'scroll event'

    def on_viewer_window_destroy(self,*args):
        if debug:print "destroy signal occurred"
        gtk.main_quit()
        #sys.exit()

    def on_quit_button_clicked(self,*args):
        if debug:print "destroy signal occurred"
        gtk.main_quit()


    def on_viewer_plusbutton_clicked(self,*args):
        #print 'plusbutton pressed'
        if self.scale>=10 :
            self.scale+=10
        elif self.scale>=1 :
            self.scale+=1
        elif 0.1<=self.scale<1:
            self.scale+=0.1
        elif 0.01<=self.scale<0.1:
            self.scale+=0.01
        #print self.scale
        self.updatesbar()
        self.printer()

    def on_viewer_minusbutton_clicked(self,*args):
        #print 'minusbutton pressed'
        if self.scale>=20 :
            self.scale-=10
        elif 2<=self.scale<=10:
            self.scale-=1
        elif 0.2<=self.scale<=1:
            self.scale-=0.1
        elif 0.02<=self.scale<=0.2:#strange
            self.scale-=0.01
        else:
            print 'scale end'
            if self.scale<=0.2 :
                print 'strangeness'
            pass

        self.updatesbar()
        #print self.scale
        self.printer()

    def on_viewer_openbutton_clicked(self,*args):
        print 'openbutton pressed'
        print 'freature disabled'
        #self.filechooser.show()

    def on_toolbutton5_clicked (self,*args):
        print 'refresh pressed'
        self.printer()

    def on_filechooserdialog_file_activated(self,*args):
        print 'file choosed'
        print args

    def on_button1_clicked(self,*args):
        print 'cancel open pressed'
        self.filechooser.hide()

    def on_button2_clicked(self,*args):
        print 'open open pressed'
        self.filename=self.filechooser.get_filename()
        print self.filename
        self.filechooser.hide()
        self.viewList=PathContainer.StandardProfile(self.filename)
        self.printer()

    #def on_viewer_hscale_value_changed(self,*args):
    #def on_viewer_hscale_format_value(self,*args):
    #   print 'hscale value changed'
    #   print args

    def clearCanvas(self):
        #print 'clearing'
        #s=self.drawable_window.get_size()
        #print 'drawable window size %i %i'%(s[0],s[1])
        #self.drawable_window.draw_rectangle(self.black_gc, True, 0, 0, s[0], s[1])
        #self.drawable_window.draw_rectangle(self.black_gc, True, 0, 0, self.win_width, self.win_height)
        self.drawable_window.set_background(self.black)


        self.drawable_window.clear()

##      self.xoffset=s[0]//2
##      self.yoffset=s[1]//2
##      self.uoffset=s[0]//2+20
##      self.voffset=s[1]//2-20

    def on_viewer_hscale_change_value(self,*args):
        #print 'hscale changed'
        #print args
        val=self.hscalepos=self.hscale.get_value()
        #print self.hscalepos
        #print int(val)
        item=self.viewList[int(val)]
        #print item

        if self.type=='quad':
            self.koordtext=' x: %.2f y: %.2f u: %.2f v:%.2f'%(item[0],item[1],item[2],item[3])
        elif self.type=='double':
            self.koordtext=' x: %.2f y: %.2f '%(item[0],item[1])

        self.updatesbar()
        #self.clearCanvas()
        self.printer()
        if self.type=='double' or self.type=='quad' or self.type=='step':
            xkoord= int(self.scale*item[0])+self.xoffset
            ykoord=-int(self.scale*item[1])+self.yoffset
            k=5
            self.drawable_window.draw_line(self.gc,xkoord+k,ykoord+k,xkoord-k,ykoord-k)
            self.drawable_window.draw_line(self.gc,xkoord-k,ykoord+k,xkoord+k,ykoord-k)
        if  self.type=='quad'or self.type=='step' :
            ukoord= int(self.scale*item[2])+self.uoffset
            vkoord=-int(self.scale*item[3])+self.voffset
            k=5
            self.drawable_window.draw_line(self.gc,ukoord+k,vkoord+k,ukoord-k,vkoord-k)
            self.drawable_window.draw_line(self.gc,ukoord-k,vkoord+k,ukoord+k,vkoord-k)

            self.drawable_window.draw_line(self.gc,xkoord,ykoord,ukoord,vkoord)

    def updatesbar(self):
        t='scale: ' + str(self.scale) + ' Koord: ' + self.koordtext
        self.statusbar.push(self.context_id,t )



    def __init__(self,contentList,type):
        #def thready(args):
        #   print 'printer thread running'
        #   while True:
        #       print 'not reprinting'
        #       #self.printer()
        #       time.sleep(1)

        print 'creating Viewer'
        self.debug=False
        self.scale=1.0
        self.xoffset=20
        self.yoffset=100
        self.smart=True

        #print 'content: ',
        #print viewList
        self.viewList=[]
        self.viewList[:]=contentList

        self.type=type

        if self.type=='double':
            self.printer=self.drawDoubleColumn
        elif self.type=='quad':
            self.printer=self.drawQuadColumn
        elif self.type=='step':
            #for future addition
            self.printer=self.drawQuadColumn
        else:
            self.printer=None

        if len(self.viewList) > 10000:
            #behave smart
            print ' make the viewer smarter'

        self.koordtext=''

        self.tree = gtk.glade.XML('gui/viewer.glade')

        # Connect the glade signals handlers to the python callbacks
        self.tree.signal_autoconnect(self)
        self.filechooser=self.tree.get_widget("filechooserdialog")
        self.filechooser.hide()

        self.statusbar=self.tree.get_widget("viewer_statusbar")
        self.context_id = self.statusbar.get_context_id('hallo context')
        self.statusbar.push(self.context_id, 'hallo')

        #self.dialog1 = self.tree.get_widget('dialog1')
        #self.button1 = self.tree.get_widget('button1')

        self.hscale=self.tree.get_widget("viewer_hscale")
        #self.hscale.set_update_policy(gtk.UPDATE_CONTINUOUS)
        self.hscale.set_range(0,100)
        self.hscale.set_increments(1,1)
        #self.analyseTree(self.tree)

        #print 'viewList'
        #print self.viewList

        self.drawingarea=self.tree.get_widget("viewer_drawingarea") #'gtk.DrawingArea' contains a Window
        #print self.drawingarea
        self.drawable_window=self.drawingarea.window

        #print self.drawable_window
        #print self.drawable_window.get_colormap()
        #s=self.drawable_window.get_size()
        #self.xoffset=s[0]//2
        #self.yoffset=s[1]//2
        #self.uoffset=s[0]//2+20
        #self.voffset=s[1]//2-20

        #self.drawingarea.set_size_request(800,600)

        #red=gtk.gdk.Color(red=0xFFFF, green=0, blue=0, pixel=0xFFFF)
        #blue=gtk.gdk.Color(red=0, green=0, blue=0xFFFF, pixel=0)
        #black=gtk.gdk.Color(red=0x00, green=0xFFFF, blue=0xFFFF, pixel=0)

        m=self.drawable_window.get_colormap()
        #colour = m.alloc_color("red")
        blue = m.alloc_color(0, 0, 65535)
        red = m.alloc_color(0xFFFF, 0, 0)
        green = m.alloc_color(0,0xFFFF, 0)
        black = m.alloc_color(100, 100,100)
        white = m.alloc_color(0xFFFF, 0xFFFF,0xFFFF)

        self.black=black
        self.gc = self.drawable_window.new_gc(foreground=white)
        self.xy_gc = self.drawable_window.new_gc(foreground=red)
        self.uv_gc = self.drawable_window.new_gc(foreground=blue)
        self.black_gc = self.drawable_window.new_gc(foreground=black)

        self.drawable_window.set_background(self.black)


        #self.drawingarea.set_redraw_on_allocate(False)
        #self.gc = gtk.gdk.GC(self.drawable_window,foreground=blue,background=blue)

        #color = gtk.gdk.Color(red=0xFFFF, green=0xFFFF, blue=0xFFFF, pixel=0)
        #wcolor = gtk.gdk.Color(red=0x0, green=0xFFFF, blue=0x0, pixel=1)
        #self.white_gc = self.drawable_window.new_gc(foreground=white)
        #self.red_gc = self.drawable_window.new_gc(foreground=red)
        #self.black_gc = self.drawable_window.new_gc(foreground=black)
        #self.back_gc=self.white_gc

        #self.back_gc= self.drawable_window.new_gc(foreground=blue)
        #self.red_gc=self.back_gc

        #print self.drawable_window.get_colormap()

        #use a thread !
        if self.viewList:
            pass
            #self.printer()


        #thread.start_new_thread(thready,(None,))

        #gtk.main()
        #self.printer()
        print  'exiting init'



    def main(self):
        #self.printer()
        gtk.main()

        pass

