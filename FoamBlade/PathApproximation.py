
############################################################################
#    Copyright (C) 2007-2012 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

import sys
try:
    import scipy
    import scipy.interpolate
except:
    print 'please install python-scientific ( scipy )'
    print 'You cant use the Spline approx without this module'


try:
    import numpy
    #import Numeric
except:
    print 'please install python-numeric (numpy)'
    print 'You cant use the Spline approx without this module'

try:
    import pylab
except:
    print 'please install python-lab (pylab)'
    print "You can't use the python lab viewer without this module"


class PathApproximation(object):
    """increase or decreases the point density in a double path

    The Path is given as a (xi,yi) List. A multivariate cubic spline is used
    to calculate an amount of new equal spaced dots on the path.
    amount = 0...N ! not N-1

    To use a linear Interpolation set degree to 1
    """

    def __init__(self,synchlist,amount=1000,smooth=0,degree=3):
        self.amount=amount
        self.x=[]
        self.y=[]
        for i in synchlist:
            self.x.append(i[0])
            self.y.append(i[1])

        print 'Smotheness '+str(smooth)
        self.tck_xy,self.u_xy=scipy.interpolate.splprep([self.x,self.y],s=smooth,k=degree)
        self.t_space=numpy.linspace(0,1,self.amount)


    def approximate(self):
        #print t_space
        self.newxy=scipy.interpolate.splev(self.t_space,self.tck_xy)
        #print newxy
        pylab.title('green: xy')
        pylab.grid(True)
        pylab.plot(self.x,self.y,'gx',self.newxy[0],self.newxy[1],'g.')
        pylab.show()

        l=[]
        for i in range(self.amount) :
            l.append((self.newxy[0][i], self.newxy[1][i])   )
        return l

class QuadPathApproximation(object):
    """increase or decreases the point density in a synchronized path

    The Path is given as a synchronized (xi,yi,ui,vi) List
    ( x,u and y,v are synchronized). A multivariate cubic spline is used
    to calculate an amount of new equal spaced dots on the path.
    amount = 0...N ! not N-1
    """

    def __init__(self,synchlist,amount=1000,smooth=0,degree=3):

        self.amount=amount
        self.x=[]
        self.y=[]
        self.u=[]
        self.v=[]
        #print synchlist
        #print 'hallo'
        for i in synchlist:
            self.x.append(i[0])
            self.y.append(i[1])
            self.u.append(i[2])
            self.v.append(i[3])

#       x=numpy.array([0,0,1,5,4,5,3,5,3,3,3])
#       y=numpy.array([4,5,3,4,3,3,8,3,2,5,8])
#       x=[0,0,1,5,4,5,3,5,3,3,3]
#       y=[4,5,3,4,3,3,8,3,2,5,8]
        #print x

        self.x=numpy.array(self.x)
        self.y=numpy.array(self.y)

        print 'Smotheness '+str(smooth)
        self.tck_xy,self.u_xy=scipy.interpolate.splprep([self.x,self.y],k=degree,s=smooth)
        self.tck_uv,self.u_uv=scipy.interpolate.splprep([self.u,self.v],k=degree,s=smooth)
        self.t_space=numpy.linspace(0,1,self.amount)
        #self.t_space=Numeric.linspace(0,1,self.amount)


    def approximate(self):
        #print t_space
        self.newxy=scipy.interpolate.splev(self.t_space,self.tck_xy)
        self.newuv=scipy.interpolate.splev(self.t_space,self.tck_uv)
        #print newxy

        try:
            pylab.title('green: xy , red:uv ')
            pylab.plot(self.x,self.y,'gx',self.newxy[0],self.newxy[1],'g.',self.u,self.v,'mx',self.newuv[0],self.newuv[1],'m.')
            pylab.show()
        except:
            print 'Error in calling Pylab'


        l=[]
        for i in range(self.amount) :
            l.append((self.newxy[0][i],
                self.newxy[1][i],
                self.newuv[0][i],
                self.newuv[1][i]))
        return l

if __name__=='__main__':


    list=[[0.0, 0.0, 0.0, 0.0], [0.0, -10.0, 0.0, -10.0], [100.0, -10.0, 100.0, -10.0], [100.0, 0.0, 100.0, 0.0], [100.0, -40.0, 100.0, -40.0], [90.0, -40.0, 90.0, -40.0], [90.0, -30.0, 90.0, -30.0], [80.0, -40.0, 80.0, -40.0], [80.0, -30.0, 80.0, -30.0], [70.0, -40.0, 70.0, -40.0], [70.0, -30.0, 70.0, -30.0], [60.0, -30.0, 60.0, -30.0], [60.0, -40.0, 60.0, -40.0], [50.0, -40.0, 50.0, -40.0], [50.0, -30.0, 50.0, -30.0], [40.0, -30.0, 40.0, -30.0], [40.0, -40.0, 40.0, -40.0], [30.0, -40.0, 30.0, -40.0], [30.0, -30.0, 30.0, -30.0], [20.0, -30.0, 20.0, -30.0], [10.0, -40.0, 10.0, -40.0], [0.0, -40.0, 0.0, -40.0], [0.0, 0.0, 0.0, 0.0]]


    p=PathApproximation(list,2)

