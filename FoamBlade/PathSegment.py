# -*- coding: utf-8 -*-

############################################################################
#    Copyright (C) 2007-2012 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################


#splprep(x, w=None, u=None, ub=None, ue=None, k=3, task=0, s=None, t=None, full_output=0, nest=None, per=0, quiet=1)
#    Find the B-spline representation of an N-dimensional curve.

#help(scipy.interpolate.splint)



#spalde(x, tck)
#    Evaluate all derivatives of a B-spline.
#-> maximum Speed

#check if the modules exist !!!
import numpy
import scipy

from PathContainer import *

class PathSegment(object):
    """Describes a path segment that can be put together with other segments
    to form a complete path

    PathSegments never change. They return a new PathSegment instead of changing their data
    """

    #Todo?
    #Speed Condition ={ left, right, middle, leftmach, rightmach, x}
    #adjusts speeds and pwm
    #checks if adjusted path is cutable in the machine
    #only relative coordiantes ?
    # TODO: dispace only if z coordinates are not zero
    # TODO: Get multiple vtk viewers running
    # TODO: Get multiple pl viewers running
    # TODO: Check that the original Path segment is not changed -> see array references, etc
    # todo: Check if a path contains identical nearby points

    #Constructor
    def __init__(self,arg=None, p=None, q=None, t=None):

        # PortalLeft----BlockLeft----BlockRight----PortalRight
        #    o              p             q             r

        self.p=None # Matrix (N,3) Path at left block
        self.q=None # Matrix (N,3) Path at right block

        #self.o=None # Matrix (N,3) Path at left portal
        #self.r=None # Matrix (N,3) Path at right portal

        #displaced paths
        #self.e=None  # Matrix (N,3)
        #self.f=None  # Matrix (N,3)

        #times for each segment in us
        # t per Element -> or speed in mm/s on which side ? maybe two speeds ?
        # ?
        self.t=None

        # Heat -> pwm Values
        # self.h

        #self.amount=0 # amount of points

        if (arg==None and p==None and q==None and  t==None):
            print "Error: No arguments specified"

        elif isinstance(arg,Quad):
            quad=arg

            cl=numpy.array(quad.getContentList())
            (length,dim)=cl.shape
            print "Converted from Quad: N=%i M=%i"%(length, dim)

            self.p=numpy.zeros((length,3), dtype=float)
            self.q=numpy.zeros((length,3), dtype=float)

            machine_width=100
            self.p[: ,0]=cl[: ,0]
            self.p[: ,1]=cl[: ,1]
            self.p[: ,2]=numpy.zeros(length,dtype=float)

            self.q[: ,0]=cl[: ,2]
            self.q[: ,1]=cl[: ,3]
            self.q[: ,2]=numpy.array([machine_width] * length,dtype=float)

            self.t=numpy.zeros(length, dtype=float)

            #fixme
            #self.updateTimes(1.3, 'max')

            self.updateSplineParam()

        elif isinstance(arg,PathSegment):
            # just clone
            self.p=arg.p.copy()
            self.q=arg.q.copy()
            self.t=arg.t.copy()
            assert ( self.p.shape[0] == self.q.shape[0] == self.t.shape[0] )

        elif (p!=None and q!=None and  t!=None):
            self.p=p.copy()
            self.q=q.copy()
            self.t=t.copy()
            #print t.shape
        else:
            print ("Error: Construction failed")

    #Special Methods

    #a+b==a.__add__(b)
    def __add__(self,b):
        p=numpy.append ( self.p, b.p, 0 )
        q=numpy.append ( self.q, b.q, 0 )
        t=numpy.append ( self.t, b.t, 0 )

        return PathSegment(p=p,q=q,t=t)

    def dimension(self):
        pxmax= max(self.p[:,0])
        pxmin= min(self.p[:,0])
        pymax= max(self.p[:,1])
        pymin= min(self.p[:,1])
        pzmax= max(self.p[:,2])
        pzmin= min(self.p[:,2])

        qxmax= max(self.q[:,0])
        qxmin= min(self.q[:,0])
        qymax= max(self.q[:,1])
        qymin= min(self.q[:,1])
        qzmax= max(self.q[:,2])
        qzmin= min(self.q[:,2])

        print "Dimensions:"
        ps= "  Right(q): Xmax %8.3f Xmin %8.3f Ymax %8.3f Ymin %8.3f Zmax %8.3f Zmin %8.3f"%\
            (pxmax, pxmin, pymax, pymin, pzmax, pzmin)
        qs= "  Left (q): Xmax %8.3f Xmin %8.3f Ymax %8.3f Ymin %8.3f Zmax %8.3f Zmin %8.3f"%\
            (qxmax, qxmin, qymax, qymin, qzmax, qzmin)
        print ps
        print qs

    def getBoundingBox(self):
        xmax= max( max(self.p[:,0]), max(self.q[:,0]) )
        xmin= min( min(self.p[:,0]), min(self.q[:,0]) )
        ymax= max( max(self.p[:,1]), max(self.q[:,1]) )
        ymin= min( min(self.p[:,1]), min(self.q[:,1]) )
        zmax= max( max(self.p[:,2]), max(self.q[:,2]) )
        zmin= min( min(self.p[:,2]), min(self.q[:,2]) )

        return (xmax, xmin, ymax, ymin, zmax, zmin)

    #Methods

    def getAmount(self):
        assert ( self.p.shape[0] == self.q.shape[0] == self.t.shape[0] )
        return self.p.shape[0]

    def getDuration(self):
        """computes cut duration
        """
        if 0 in self.t:
            print 'warning path contains zero time'

        s=sum( self.t)
        print 'Cut Duration is %f us = %4.3f s '%(s,s*1e-6)
        return s

    def calculatePathLengths(self):
        """geometrical length of the path splines
        returns length of p and q
        BUG: maybe calculates the wrong length

        call update updateSplineParam() in advance
        """
        self.updateSplineParam()

        #splint(a, b, tck, full_output=0)
        #    Evaluate the definite integral of a B-spline.
        #-> length
        if self.tck_p==None or self.tck_q==None:
            print 'Error: tck_p or tck_q missing rerun updateSplineParam'
            return
        lp=[]
        lq=[]
        lp= scipy.interpolate.splint( 0, 1, self.tck_p)
        lq= scipy.interpolate.splint( 0, 1, self.tck_q)
        print 'Path length (linear) is right %f mm and left %f mm'%(lp[0],lq[0])
        print 'Average distance between points is right %f mm and left %f mm'%\
            (lp[0]*1.0/self.getAmount(),lq[0]*1.0/self.getAmount())

        return lp,lq

    def updateSplineParam(self, degree=1, smooth=0):
        synchlist=[] #todo
        #amount=1000

        spline_smooth=smooth
        spline_degree=degree

        #print 'Updating Spline: Smotheness %4f Degree %i Synchpoints %i'%\
        #    (spline_smooth,spline_degree,len(synchlist))

        if spline_degree>= self.getAmount():
            print 'To interplate you have to use more points (current %i) than the degree of your spline (%i).\n'%(self.getAmount(),spline_degree) +\
                'Please choose a larger point amount or use a smaller degree'
            return None

        self.tck_p,self.u_p=\
                scipy.interpolate.splprep( numpy.transpose(self.p) ,
                k=spline_degree,s=spline_smooth)
        self.tck_q,self.u_q= \
                scipy.interpolate.splprep( numpy.transpose(self.q),
                k=spline_degree,s=spline_smooth)

    def interpolate(self, new_amount, degree=1, smooth=0):
        """uses multivariate B-splines directly
        interpolates to gain a higher resolution
        """

        self.updateSplineParam(degree, smooth)

        if self.tck_p==None or self.tck_q==None:
            print 'tck_p or tck_q missing rerun updateSplineParam'
            return

        #load splineparam from somewhere

        t_space= numpy.linspace(0,1,new_amount)

        tp = scipy.interpolate.splev(t_space,self.tck_p)
        p= numpy.transpose(tp)
        tq = scipy.interpolate.splev(t_space,self.tck_q)
        q = numpy.transpose(tq)

        #print "New point amount %i"%self.getAmount()

        #todo: Fix t
        t=numpy.zeros(new_amount, dtype=float)

        return PathSegment(p=p,q=q,t=t)

#    def getSubSegmentLength(self,r,i):
#        """return length part of part i of r={p,q}
#        """
#        if i<= self.amount -1 -1: # one for N-1 and one due to the difference
#            s1= numpy.array( [r.x[i], r.y[i] ,r.z[i] ] )
#            s2= numpy.array( [r.x[i+1], r.y[i+1] ,r.z[i+1] ] )
#            s=s2-s1
#            return  math.sqrt ( numpy.inner ( s , s) )
#        else:
#            print 'value %i for i is to high'%i
#            return None

#    def updateTimes(self,speed,cond):
#        """
#        speed: speed in mm/s
#        cond: speed condition ={}
#        """
#        if cond=='max':
#            for i in numpy.arange(self.amount-1):
#                #a,b=(self.getSubSegmentLength(self.p,i),
#                #self.getSubSegmentLength(self.q,i) )
#                x= max (self.getSubSegmentLength(self.p,i),
#                    self.getSubSegmentLength(self.q,i) ) / speed
#                y=round( x*1e6)
#                self.t[i]=y

    def plView(self):
        """Opens a simple Python-Lab viewer
        """
        pylab.xlabel('xu')
        pylab.ylabel('yv')
        pylab.title('Quad Plot xy:green uv:red')
        pylab.plot(self.p[:,0], self.p[:,1],'g.-', self.q[:,0], self.q[:,1],'m.-')
        pylab.grid(True)
        pylab.show()

    def plViewFlat(self):
        """Opens a simple Python-Lab viewer
        """
        a=numpy.arange(self.getAmount())
        pylab.xlabel('steps')
        pylab.ylabel('yvuv')
        pylab.title('Quad Plot xy:green uv:red')
        pylab.plot(     self.p[:,0],'rx-', \
                        self.p[:,1], 'gx-', \
                        self.p[:,2], 'bx-', \

                        self.q[:,0], 'r+-', \
                        self.q[:,1], 'g+-',\
                        self.q[:,2], 'b+-'\

                        )
        pylab.grid(True)
        pylab.show()

    def setBlockPos  (self, pz, qz):
        """Set new block z coordinates, old ones are ignored.
        Block and machine widths are assumed to be constant.
        """
        p=self.p.copy()
        q=self.q.copy()

        p[:,2]= numpy.array([pz] * self.getAmount(), dtype=float)
        q[:,2]= numpy.array([qz] * self.getAmount(), dtype=float)
        return PathSegment(p=p,q=q,t=self.t)

    #todo there is a bug somewhere here
    def adjustMachineWidth(self, pz, qz): #block_width,machine_width):
        """modifies p and q to have new z values

        use a machine that has more width than the block you want to cut
        test it (theoreticaly it works:)
        """

        # from left to right:
        #(xy)=0---block begin at dr---block end at d+dr---(uv) at dm=d+dr+dl


        #
        # two vectors p and q shall be transformed into vectors pn and qn
        # p, q, pn.z and qn.z are known
        # alpha and beta are element of R

        # pn= p+ alpha * (p-q) / |p-q| (normalized)
        #
        # qn= p+ beta  * (p-q) / |p-q| (normalized)
        #
        # ->
        # alpha = pn.z * |p-q| / (p.z - q.z)
        # beta  = pn.z * |p-q| / (p.z - q.z)
        #

        #print 'Adjusting from %f and %f to values %f and %f'%(self.p.z[0], self.q.z[0], newPZ, newQZ    )

        #pnz=newPZ
        #qnz=newQZ
        # -> use o and r

        if (type(pz)==int or type(pz)==float) and (type(qz)==int or type(qz)==float):
            None
        else:
            print("Error: Arguments other that type of float and int are not yet supported")
            return

        pn=self.p.copy()
        qn=self.q.copy()

        for i in numpy.arange(self.getAmount()):

            #o= self.o[i]
            o= pz
            p= self.p[i]
            q= self.q[i]
            #r= self.r[i]
            r= qz

            #Info p,q, pq, pn, qn are all arrays (NxN) !

            # p-q
            qp=p-q
            # |p-q|
            qpabs= math.sqrt ( numpy.inner ( qp , qp) )

            #alpha = (-o[2]+p[2]) * qpabs / (q[2] - p[2])
            #beta  = (-r[2]+p[2]) * qpabs / (q[2] - p[2])
            alpha = (-o+p[2]) * qpabs / (q[2] - p[2])
            beta  = (-r+p[2]) * qpabs / (q[2] - p[2])

            #print 'alpha %f beta %f qpabs %f'%(alpha,beta,qpabs )
            pn[i]= p + alpha * qp /qpabs
            qn[i]= p + beta * qp /qpabs

        #todo fix t
        return PathSegment(p=pn,q=qn,t=self.t)


#    def adjustMachineWidth  (self, oz, pz, qz, rz): #machineWidth, blockWidth):
#        """Set new machine and block parameters.
#
#        Block and machine widths are assumed to be constant.
#        Returns a segment with new z values
#        """
#
#        #if machineWidth==blockWidth:
#        #    return
#
#        if self.o ==None:
#            self.o=numpy.zeros((self.amount,3), dtype=float)
#        self.o[:,2]= numpy.array([oz] * self.amount, dtype=float)
#
#        self.p[:,2]= numpy.array([pz] * self.amount, dtype=float)
#        self.q[:,2]= numpy.array([qz] * self.amount, dtype=float)
#
#        if self.r ==None:
#            self.r=numpy.zeros((self.amount,3), dtype=float)
#        self.r[:,2]= numpy.array([rz] * self.amount, dtype=float)

    def tvtkView(self, showbox=None):
        #from enthought.tvtk.api import tvtk
        from enthought.tvtk.tools.visual import curve, box, vector, show
        from numpy import arange, array

        a=curve(points= ((0,0,0),(10,0,0)) , color = (1,0,0), radius=0.5 )
        a=curve(points= ((0,0,0),(0,10,0)) , color = (0,1,0), radius=0.5 )
        a=curve(points= ((0,0,0),(0,0,10)) , color = (0,0,1), radius=0.5 )

        d=curve(color = (1,1,1), radius=0.2 )
        d.append( (0,0,0))
        d.append( (100,0,0))
        d.append( (100,0,100))
        d.append( (0,0,100))
        d.append( (0,0,0))

        if self.getAmount() <1000:
            stepsize=5
        else:
            stepsize=20
        points=[]
        showwire=True # at portal
        # generate whole wire at once:
        for i in numpy.arange(self.getAmount()-stepsize-1):
            if ((i%(stepsize*2)) ==0 ):
                points.append( ( self.p[i]))
                points.append( ( self.q[i] ))
                points.append( ( self.q[i+stepsize]) )
                points.append( ( self.p[i+stepsize]))

        #print points
        curve( points=points, color = (0.6,0.6,0.6), radius=0.05 )

        if showbox==True:
            (xmax, xmin, ymax, ymin, zmax, zmin)=self.getBoundingBox()

            pb1=(
                [xmin, ymin, zmin],
                [xmax, ymin, zmin],
                [xmax, ymax, zmin],
                [xmin, ymax, zmin],
                [xmin, ymin, zmin])
            pb2=(
                [xmin, ymin, zmax],
                [xmax, ymin, zmax],
                [xmax, ymax, zmax],
                [xmin, ymax, zmax],
                [xmin, ymin, zmax]
                )
            pb3= ([xmin, ymin, zmax], [xmin, ymin, zmin])
            pb4= ([xmax, ymin, zmax], [xmax, ymin, zmin])
            pb5= ([xmax, ymax, zmax], [xmax, ymax, zmin])
            pb6= ([xmin, ymax, zmax], [xmin, ymax, zmin])

            rb=0.2
            b1=curve(points=pb1, color = (1,1,1), radius=rb )
            b2=curve(points=pb2, color = (1,1,1), radius=rb )
            b3=curve(points=pb3, color = (1,1,1), radius=rb )
            b4=curve(points=pb4, color = (1,1,1), radius=rb )
            b5=curve(points=pb5, color = (1,1,1), radius=rb )
            b6=curve(points=pb6, color = (1,1,1), radius=rb )

        cp = curve( points=self.p ,color = (0,1,0), radius=0.2 ) # green
        cq = curve( points=self.q, color = (1,0,0), radius=0.2 ) # red
        show()

    #classmethod
    def tvtkViewProcess(path, displaced, portal):
        print "Starting process viewer"
        from enthought.tvtk.tools.visual import curve, box, vector, show
        #http://code.enthought.com/projects/files/ets_api/enthought.tvtk.tools.visual.html
        from numpy import arange, array

        a=curve(points= ((0,0,0),(10,0,0)) , color = (1,0,0), radius=0.5 )
        a=curve(points= ((0,0,0),(0,10,0)) , color = (0,1,0), radius=0.5 )
        a=curve(points= ((0,0,0),(0,0,10)) , color = (0,0,1), radius=0.5 )

        d=curve(color = (1,1,1), radius=0.2 )
        d.append( (0,0,0))
        d.append( (100,0,0))
        d.append( (100,0,100))
        d.append( (0,0,100))
        d.append( (0,0,0))

        ptsw=[]

        points=[]

        if portal.getAmount() <1000:
            stepsize=5
        else:
            stepsize=20

        if portal!=None:
            showwire=True # at portal
            # generate whole wire at once:
            for i in numpy.arange(portal.getAmount()-stepsize-1):
                if ((i%(stepsize*2)) ==0 ):
                    points.append( ( portal.p[i]))
                    points.append( ( portal.q[i] ))
                    points.append( ( portal.q[i+stepsize]) )
                    points.append( ( portal.p[i+stepsize]))

            #print points
            curve( points=points, color = (0.6,0.6,0.6), radius=0.05 )

        if portal!=None:
            cp = curve( points=portal.p ,color = (0,1,0), radius=0.2 ) # green
            cq = curve( points=portal.q, color = (1,0,0), radius=0.2 ) # red

        co = curve( points=path.p, color = (1,0,1), radius=0.2 ) # pink
        cr = curve( points=path.q ,color = (1,1,0), radius=0.2 ) # yellow

        if displaced!=None:
            ce = curve( points=displaced.p, color = (0.6,1,0.6), radius=0.2 ) #liht red
            cf = curve( points=displaced.q  ,color = (1,0.6,0.6), radius=0.2 ) #light geen

        show()

    tvtkViewProcess=staticmethod(tvtkViewProcess)

    def displace(self,dxy,duv):
        """Configure a displacement on both sides xy and uv"""

        # still beta

        # Steps:
        # * Create a vector of differential steps delta_* which represents
        #   the actual direction
        # * Use a cross product to get a vector wich is perpendicular to the direction

        # todo: use the data

        p=self.p
        q=self.q

        pq=p-q

        tmpp=p[1: , : ]
        tmpq=q[1: , : ]

        tmp3=numpy.zeros((1,3), dtype=float)
        tmp4=numpy.zeros((1,3), dtype=float)

        tmp3=p[-1 , : ]
        tmp4=q[-1 , : ]

        shiftedp=numpy.vstack((tmpp,tmp3))
        shiftedq=numpy.vstack((tmpq,tmp4))

        delta_p=shiftedp-p[0: , : ]
        delta_q=shiftedq-q[0: , : ]

        # todo repeat the last point or find a better solution
        # bugfix: repeat the last one
        delta_p[-1 , : ]=p[-2 , : ]
        delta_q[-1 , : ]=q[-2 , : ]

        print delta_p.shape

        cross_p=numpy.zeros((delta_p.shape[0],3), dtype=float)
        cross_q=numpy.zeros((delta_q.shape[0],3), dtype=float)

        for i in range(delta_p.shape[0]):
            #cross product
            cp=numpy.cross( delta_p[i], pq[i]  )
            cq=numpy.cross( delta_q[i], pq[i]  )
            # normalize and multiply
            cross_p[i] = cp / numpy.sqrt( numpy.vdot(cp,cp) ) * dxy
            cross_q[i] = cq / numpy.sqrt( numpy.vdot(cq,cq) ) * duv

        pn=cross_p.copy()+self.p
        qn=cross_q.copy()+self.q

        #todo fix t
        return PathSegment(p=pn, q=qn,t=self.t)


    def export_gcode(self,filename):
        # branched from the PathContainer exporter
        """writes a IEC 66025 NC file
        Untested and accordingly not very correct
        """
        f=file(filename,'w+')
        os.chmod(filename,0777) # octal nubmer -> leading 0
        print( '\nWriting NC data to "' +filename+'"')

        #f.write("G21 (Millimeters)\n")
        #f.write("F500 (FeedRate)\n")
        #f.write("G64 P0.001    (Digital Output Control???)\n")
        #f.write("G0 X0 Y0 U0 V0\n" )
        #f.write("(MSG, Press S)\n")
        #f.write("M00 (Pause - > Press S)\n")

        #fits to lotar sammels paeamble

        preamble_sammel="""(test XY UV emc2 4 axis)
G17.1 G21 G90 G61 G54
(AXIS,XY_Z_POS,5)
(AXIS,UV_Z_POS,100)
(AXIS,GRID,5)
G0 x0 y0 u0 v0
G0 x5 y5
"""

# copied from the linuxcnc foam.ngc example
        preamble="""(XY UV emc2 4 axis )

G17 (select xy plane)
G21 (millimeter)
G90 (absolute distance mode)
G64 (continuous path mode)
G54 (coordinate system)

(AXIS,XY_Z_POS,5)
(AXIS,UV_Z_POS,30)
(AXIS,GRID,5)

"""
#G0 x0 y0 u0 v0


#(square uv 45 deg turned offset 10mm to zero )
#G0 x5 y5
#G0 x10 y10  u22.5 v22.5
#G1 x10 y35 u10 v35 F200
#G1 x10 y60 u22.5 v47.5
#G1 x35 y60 u35 v60
#G2 r25 x60 y35 u60 v35
#G1 x60 y10 u47.5 v22.5
#G1 x35 y10 u35 v10
#G1 X10 Y10 u22.5 v22.5
#G1 X5 Y5 U5 V5
#G0 x0 u0


        line_g1="G1 X%f Y%f U%f V%f"

        end="""G0 x0 u0
G0 x0 y0 u0 v0
M30
"""


        f.write(preamble)
        #todo use displaced arrays
        for i in numpy.arange(self.getAmount()):
            #print self.e[i]
            #print self.o.x[i]
            #s=line_g1%(self.e[i,0], self.e[i,1], self.f[i,0], self.f[i,1])
            #s=line_g1%( self.o.x[i], self.o.y[i], self.r.x[i], self.r.y[i])
            s=line_g1%(self.p[i,0], self.p[i,1], self.q[i,0], self.q[i,1])
            if i==0:
                s+=" F500"
            f.write(  s +'\n')

        f.write(end)

        f.close()
        print( 'wrote data file "' +filename+'" with ' + str (self.getAmount()) +' enteries')

# Magic path enhancer

    #Todo: More intelligence needed
    def getBoundingPath(self,distance):
        d=distance
        (xmax, xmin, ymax, ymin, zmax, zmin)=self.getBoundingBox()
        bp=numpy.array( [
            [xmin-d, ymin-d, zmin],
            [xmax+d, ymin-d, zmin],
            [xmax+d, ymax+d, zmin],
            [xmin-d, ymax+d, zmin],
            [xmin-d, ymin-d, zmin],
            ], dtype=float)
        bq=numpy.array( [
            [xmin-d, ymin-d, zmax],
            [xmax+d, ymin-d, zmax],
            [xmax+d, ymax+d, zmax],
            [xmin-d, ymax+d, zmax],
            [xmin-d, ymin-d, zmax],
            ], dtype=float)

        bt=numpy.zeros( 5 ,dtype=float)

        return PathSegment(p=bp,q=bq,t=bt)




