# -*- coding: utf-8 -*-

############################################################################
#    Copyright (C) 2007-2012 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

"""Interactive FoamBlade Session

This script should be started in an interactive mode try
$./interactive.sh or
$python -i FoambladeInteractive.py
"""

import os
import os.path
import time
import sys
import string
#print sys.argv

import PathContainer
#from PathContainer import *
from Viewer import Viewer
from dxftools import *
import PathSegment
from PathSegment import *

print
print '********************************************'
print '*  starting interactive Foamblade session  *'
print '********************************************'
print 'press Ctrl + d to exit'


if os.path.isfile('UserFunctions.py'):
    from UserFunctions import * 
    print   'loading user init file UserFunctions.py'
    #execfile('UserFunctions.py')
    demo()
    

#this loads an interactive session in the former foamblade gui 
#mostly deprecated

sessionfile=''
if len(sys.argv)>=2:
    arg=sys.argv[1]
    if os.path.isfile(arg):
        if string.lower(arg[-5:]) == '.quad':
            print "loading quad k=Quad('%s') "%(arg)
            print "k=Quad('%s') "%(arg)
            #print 'argument for loading '+str(arg)

            #PathContainer.PathContainer.setSessionFile(arg)
            PathContainer.setSessionFile(arg)
            #k=PathContainer.Quad(arg)
            k=Quad(arg)

print
print 'Your turn.'
print

