# -*- coding: utf-8 -*-

############################################################################
#    Copyright (C) 2007-2012 Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

"""     user file
"""

import PathContainer
#from PathContainer import *
from Viewer import Viewer
from dxftools import *
import PathSegment
from PathSegment import *


def test_1():
    print ">>> Test Code"

    global s1
    global s2
    global s3
    global s4
    global s5
    global s6
    global s7

    s1=Double('Shapes/rg15.dat')
    s2=s1.convertToQuad()
    s2.scale(100,100,50,50)

    s5=PathSegment(s2)

    s6=s5.setBlockPos(-10, 110)

    s6.tvtkView(showbox=True)

def test_2():
    print ">>> Test Code"

    s1=Double('Shapes/rg15.dat')
    s2=s1.convertToQuad()
    s2.scale(200,200,50,50)
    s2.addOffset(0,0,150,0)
    s2.rotate(0,0,100,0,0,10)

    s3=PathSegment(s2)
    #s10.mayaviView()
    #s10.plView()

    s3.dimension()
    s3.calculatePathLengths()

    s4=s3.interpolate(200)
    s4.dimension()
    s4.calculatePathLengths()

    s5=s4.setBlockPos(0, 100)
    s6=s5.adjustMachineWidth(-50, 150)
    s6.dimension()
    s6.calculatePathLengths()
    b=s6.getBoundingPath(10)
    s7=s6+b
    s7.tvtkView()

def test_3():
    print ">>> Test Code"
    global s1
    global s2
    global s10

    s1=Double('Shapes/rg15.dat')
    s2=s1.convertToQuad()
    s2.scale(200,200,50,50)
    s2.addOffset(0,0,150,0)
    s2.rotate(0,0,100,0,0,10)
    s3=PathSegment(s2)
    s4=s3.interpolate(1000)
    s5=s4.setBlockPos(0, 100)
    #s10.displace(-1,-1)
    s6=s5.displace(-3,-3)
    s7=s6.adjustMachineWidth(-10, 110)
    s7.tvtkView()

def test_4():
    print ">>> Test Code"

    s1=Double('Shapes/rg15.dat')
    s2=s1.convertToQuad()
    s2.scale(200,200,50,50)
    s2.addOffset(0,0,150,0)
    s2.rotate(0,0,100,0,0,10)
    s3=PathSegment(s2)
    s4=s3.interpolate(1000)
    s5=s4.setBlockPos(0, 100)
    s6=s5.displace(-3,-3)
    s7=s6.adjustMachineWidth(-10, 110)
    s7.export_gcode("test.ngc")


def test_5():
    print ">>> Test Code"

    s1=Double('Shapes/rg15.dat')
    s2=s1.convertToQuad()
    s2.scale(50,50,100,100)
    s2.addOffset(-50,0,-100,0)

    #this generates a unsynchronized path
    s3=s2.repeatQuad( 3, 0,20,0,20)

    #s2.rotate(0,0,100,0,0,10)
    s3b=PathSegment(s3)
    s4=s3b.interpolate(1000)
    s5=s4.setBlockPos(0, 100)
    #s10.displace(-1,-1)
    s6=s5.displace(-0.5,-0.5)
    s7=s6.adjustMachineWidth(-10, 110)
    s7.tvtkView()

def test_6():
    print ">>> Test Code"

    s1=Double('Shapes/rg15.dat')
    s2=s1.convertToQuad()
    s2.scale(80,80,100,100)
    s2.addOffset(-80,0,-100,0)

    c1=Double('Shapes/clarky.dat')
    c2=c1.convertToQuad()
    #c2.plView()
    c2.scale(50,50,100,100)
    c2.addOffset(-50,30,-100,30)

    n1=Double('Shapes/naca.dat')
    n2=c1.convertToQuad()
    n2.scale(30, 30, 30 ,30)
    n2.addOffset(-50,60,-50,60)

    m2=c1.convertToQuad()
    m2.scale(30, 30, 8 ,8)
    m2.addOffset(-50,90,-50,90)

    s10=PathSegment(s2)
    s11=s10.interpolate(1000)
    s12=s11.setBlockPos(0, 100)

    c10=PathSegment(c2)
    c11=c10.interpolate(1000)
    c12=c11.setBlockPos(0, 100)

    n10=PathSegment(n2)
    n11=n10.interpolate(1000)
    n12=n11.setBlockPos(30, 60)

    #n2.scale(0.5, 1.2, 1.1 ,1.2) #
    #n2.addOffset(0,0,0,0) # relative
    m10=PathSegment(m2)
    m11=m10.interpolate(1000)
    m12=m11.setBlockPos(50, 90)

    a12= s12+ c12+ n12 + m12

#    c11.tvtkView()
#    a11.tvtkView()

    a15=a12.adjustMachineWidth(-10, 110)

    a15.export_gcode("test.ngc")

    PathSegment.tvtkViewProcess(path=a12, displaced=None, portal=a15)

def test_7():
    #p=scanDXF("../dxf2quad/paths/allObjects.dxf")
    p=scanDXF("../dxf2quad/paths/extra-complete.dxf")
    s=PathSegment(p)
    s.dimension()
    s.calculatePathLengths()
    PathSegment.tvtkView(s)


def test_9():
    print ">>> Test Code"

    global s1
    global s2
    global s3
    global s4
    global s5
    global s6
    global s7
    global s8
    global s10

    s1=Double('Shapes/rg15.dat')
    s2=s1.convertToQuad()
    s2.scale(200,200,100,100)
    s2.addOffset(0,0,150,0)
    s2.rotate(0,0,100,0,0,5)
    s5=PathSegment(s2)


    s5.calculatePathLengths()

    #fixme
    #s5.tvtkView()

    sx=s5.interpolate(1000, degree=3)
    s6=PathSegment(sx) #test copying

    #s6.plViewFlat()
    #fixme
    #s6.plView()

    s6.calculatePathLengths()

    s7=s6.setBlockPos(-10, 50)

    #fixme
    #s7.plViewFlat()

    s7b=s7.displace( 2, -6)
    s7b.plViewFlat()

    s8=s7b.adjustMachineWidth(-50, 80)

    s8.export_gcode("test.ngc")

    PathSegment.tvtkViewProcess(path=s7, displaced=s7b, portal=s8)

def demo():
    #print 'loading a few files, just for lazyness:'
    print ">>> q=QuadColumn('Shapes/rg15.quad')"
    q=QuadColumn('Shapes/rg15.quad')
    
    #print ">>> r=DoubleColumn('Shapes/pw51.pro')"
    #r=DoubleColumn('Shapes/pw51.pro')
    print ">>> d=DoubleColumn('Shapes/rg15.dat')"
    d=DoubleColumn('Shapes/rg15.dat')
    
  
    print "Available tests:"
    print "test_1() : Load an RG15 do some stuff and load viewer."
    print "test_2() : Load an RG15 do some stuff, toy with dimensions and load viewer."
    print "test_3() : Load an RG15 do some stuff, displace and load viewer."
    print "test_4() : Generate Path and save as test.ngc"
    print "test_5() : Generate repeated Path and save as test.ngc"
    print "test_6() : load a few profiles, add them and view"
    print "test_7() : load a DXF, convert and view"
    print "test_9() : Everything"
    print ""
    
    choice=raw_input("Execute a test/demo script? Press n or 1..9 : ")
    print choice
    if choice=="n" or choice=="0":
        pass
    elif choice=="1":
        test_1()
    elif choice=="2":
        test_2()
    elif choice=="3":
        test_3()
    elif choice=="4":
        test_4()
    elif choice=="5":
        test_5()
    elif choice=="6":
        test_6()
    elif choice=="7":
        test_7()
    elif choice=="9":
        test_9()
    else:
        pass




