# -*- coding: utf-8 -*-

############################################################################
#    Copyright (C) 2007-2012 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################


"""Container classes for input Files and read Methods

"""


import re
import sys
import math
import os

from Viewer import Viewer

from  Algorithms import LinearInterpolation
from PathApproximation import *

#from PyVisualViewer import *

#import numpy



#cuurently only used for the plViewer
#import pylab
#from pylab import plot, show, ylim, yticks
#from matplotlib.numerix import sin, cos, exp, pi, arange


class PathContainer(object):
    """Standard methods for it's subclasses

    Every defined Function applies to all Subclasses:
    Quads, Doubles, StepLists, CutPathes

    abstract class
    """

    def __init__(self,arg):
        """Eats filenames, ContentLists, PathContainers, 'edit' and 'empty'
        """
        self.generator=self.nextGenerator()
        self.contentList=[]
        self.generator=None
        self.info=''
        self.sep='\t\t'
        self.debug=False
        self.filename=None


        #ugly
        sessionfile=None    #is modified by static methods
        # access this with: PathContainer.PathContainer.setSessionFile(arg)

        self.FoamBlade_GUI=None
        global FoamBlade_GUI
        FoamBlade_GUI=None

        if isinstance(arg,list):
            self.contentList[:]=arg
            if self.debug:
                print 'recceived contentList with ' + str(len(self.contentList)) +' enteries'

        elif  isinstance(arg,PathContainer):
            self.contentList[:]=arg.getContentList()
            if self.debug:
                print 'recceived Path Contaier with ' + str(len(self.contentList)) +' enteries'
        elif arg=='edit':
            #self.contentList=[]
            self.edit()
        elif arg=='empty':
            pass
        else:
            self.readFromFile(arg.strip())

    ##static mehtnods
    def getSessionFile():
        if 'sessionfile' in globals():
            return sessionfile

    def setSessionFile(file):
        global sessionfile
        sessionfile=file

    def setGUI(gui):
        # gui should be a reference to the fb gui
        global FoamBlade_GUI
        FoamBlade_GUI=gui
        #print 'Path Container gui is ' + str(gui)

    def getGUI():
        return FoamBlade_GUI
    FoamBlade_GUI=None
    getSessionFile=staticmethod(getSessionFile)
    setSessionFile=staticmethod(setSessionFile)
    setGUI=staticmethod(setGUI)
    getGUI=staticmethod(getGUI)

    ##class methods

    def nextGenerator(self):
        """for internal use
        """
        for iter in self.contentList:
            yield iter

    def getNextItem(self):
        """for internal use
        uses a generator to get the next in the list
        """
        return self.generator.next()

    def getContentList(self):
        """Return the underlying content List of the container
        """
        return self.contentList

    def getListSize(self):
        return len(self.contentList)

    def getItem(self,num=0):
        return self.contentList[num]

    def printContent(self):
        """print information and the contentList
        """
        print self.info
        for i in  range(len(self.contentList)):
            print str(i)+' '+ str(self.contentList[i])

    def writeToFile(self,filename):
        f=file(filename,'w+')
        try:
            #this will fail if the file is not yours
            os.chmod(filename,0777) # octal nubmer -> leading 0
        except:
            pass


        if self.debug:
            print 'writing data to "' +filename+'"'

        for i in self.contentList:
            f.write(  self.itemToString(i) +'\n')
        f.close()
        print 'wrote data file "' +filename+'" with ' + str (len(self.contentList)) +' enteries'

    def save(self):
        """uses the filename the path was read from, to save it back
        """
        if self.filename!=None:
            self.writeToFile(self.filename)
        #elif PathContainer.PathContainer.getSessionFile()!=None:
        elif PathContainer.getSessionFile()!=None:
            print 'The Path you want to save has no own filename set.'
        #   print 'However, the filename "' + PathContainer.PathContainer.getSessionFile()+'" from the interactive session'
            print 'However, the filename "' + PathContainer.getSessionFile()+'" from the interactive session'
            print 'is set up. Use isave() to use this filename or use writeToFile(<name>)'
            print 'and specify another filename'
        else:
            print 'Sorry no filename is saved: use writeToFile(<name>) instead'

    def isave(self):
        """uses the filename that was given to the session, to save it back
        """
        if PathContainer.getSessionFile()!=None:
            self.writeToFile(PathContainer.getSessionFile())
        else:
            print 'no filename available'

    def readFromFile(self,filename):
        if self.debug:
                print 'reading data from ' +filename
        self.filename=filename
        try:
            self.file=open(filename)
        except:
            print 'Couldn\'t open file'
            return

        self.contentList=[]
        self.info=''
        j=0
        for i in self.file:
            #try:
            item=self.stringToItem(i)
            if    item:
                self.contentList.append(item)
            #except:
            else:
                if not re.match('\S+',i):
                    pass #only spaces
                else:
                    print '\tignored line: '+str(j) +'"' + i.strip() +'"'
                    self.info+='INFO: ' +i.strip()
            j+=1
        print '\tread data file '+ self.filename+' with ' + str (len(self.contentList)) +' enteries'

    def getFilename(self):
        """prints and returns the Filename the Path was read from
        """
        if self.filename!=None:
            print self.filename
        else:
            print 'no filename available'
        return self.filename


    def itemToString(self,nothing):
        """abstract!
        converts a list item into a string
        """
        print 'Tried to make an object form an abstract class'
        sys.exit()

    def stringToItem(self,nothing):
        """abstract
        tries to convert a list item into a string
        """
        print 'Tried to make an object form an abstract class'
        sys.exit()

    def appendItem(self,item):
        """appends an item to the end
        """
        #self.contentList.extend(l)
        self.contentList+=item

    def appendPath(self,path):
        """append the given Path to the Path
        """
        copy=path.getContentList()
        self.contentList+=copy[:]

    def reverse(self):
        """reverse the complete Path
        """
        self.contentList.reverse()


    #def setContent(self,cont):
    #   self.contentList[:]=cont

    def edit(self,*arg):
        """calls an external editor (gedit)

        Uses an external editor to edit the file represetatation of the path.
        If you want you can give the editor to use as argument, maybe 'kwrite'
        """
        os.system(' rm -f Shapes/path.temp')
        self.writeToFile('Shapes/path.temp')
        if arg:
            editor=arg[0]
        else:
            editor='gedit'
        os.system( editor + ' Shapes/path.temp')
        print 'rereading path'
        self.readFromFile('Shapes/path.temp')


class DoubleColumn(PathContainer):
    """A List with two values -> two columns

    Fits to most standard foils.
    Alias name is Double
    """

    def __init__(self,arg,Scale=1):
        self.Scale=Scale

        if Scale!=1:
            print 'creating DoubleColumn with scale factor '+str(self.Scale)

        PathContainer.__init__(self,arg)


    def itemToString(self,item):
        s=str(item[0])+self.sep+str(item[1])
        return s

    def stringToItem(self,s):
        #r='^\s*-?\d+\.\d+\s*-?\d+\.\d+'
        r='^(\s*-?\d+(\.\d+)?){2}'
        if re.match(r,s):

            p=re.split(',?\s*',s,2)
            while '' in p:
                p.remove('')
            return [ float(p[0])*self.Scale ,float ( p[1])*self.Scale]
        else :
            return None

    def plView(self):
        """call the pyton lab viewer
        """
        x=[]
        y=[]

        for i in self.contentList:
            x.append(i[0])
            y.append(i[1])

        pylab.xlabel('x')
        pylab.ylabel('y')
        pylab.title('Double Plot')
        pylab.plot(x, y,'.', color='k')
        #ylim(-1,4)
        #yticks(arange(4), ['S1', 'S2', 'S3', 'S4'])
        pylab.grid(True)
        pylab.show()

    def view(self):
        """uses the built in viewer to open the quad
        """
        print len (self.contentList)
        if len (self.contentList)>=1:
            viewer = Viewer(self.getContentList(),'double')
            viewer.main()

    def scale(self,xs,ys):
        """ Scale the Double with xs and ys in x and y direction.
        """
        for i in range(len(self.contentList)):
            p=self.contentList[i]
            self.contentList[i]=( p[0]*xs, p[1]*ys)

    def splineInt(self,amount=1000,smooth=0,degree=3):
        """returns a new approximated double
        the point density is equalized
        """
        print 'old length %i' %self.getListSize()
        self.clean()
        p=PathApproximation(self.contentList,amount,smooth=smooth,degree=degree)
        a= p.approximate()
        #print a
        q=DoubleColumn(a)
        print 'new length %i' %q.getListSize()
        return q

    #self.splineApprox=self.splineInt

    def composeQuad(self,double,points=1000):
        """tries to compose Quad from two different shaped Doubles

        call q=d1.composeQuad(d2)
        Does a spline Interpolation to create two doubles with
        the same point amount and merges them together to a quad.
        The quad is returned.
        A known problem is that both sides don't synchronize well,
        when the Doubles differ much in their spape. The Doubles are
        equalized over their length and then put together. A possible
        Trick is: scale one Double in one direction, merge the quad
        and scale the direction of the side you scaled before back to
        the correct size.
        """
        l1=self.getListSize()
        l2=double.getListSize()
        leff=max(l1,l2,points)
        n1=self.splineInt()
        n2=double.splineInt()
        q=n1.mergeToQuad(n2)
        return q

    def mergeToQuad(self,double):
        """merges two doubles with the same length and returns a QuadColumn
        call q=d1.composeQuad(d2)
        """
        l1=self.getListSize()
        l2=double.getListSize()
        if l1!=l2:
            print 'The doubles should have the same length.'
            return None

        quadList=[]
        for i in range(l1):
            quadList.append( (self.contentList[i][0],self.contentList[i][1],
            double.contentList[i][0],double.contentList[i][1]) )
        quad=QuadColumn(quadList)
        return quad


    def convertToQuad(self):
        """returns a QuadColumn
        The Quad has the Double on both sides.
        """

        quadList=[]
        for i in self.contentList :
            quadList.append( [i[0],i[1],i[0],i[1]])
        quad=QuadColumn(quadList)
        return quad
    #old:
    convertToQuadColumn=convertToQuad

    def getLength(self):
        """returns Length of the path in mm
        """

        pold=self.contentList[0]
        lxy=0
        for i in range(len(self.contentList)): #-1
            p=self.contentList[i]
            lxy+= math.sqrt( (p[0]-pold[0])**2+(p[1]-pold[1])**2)
            pold=p

        print 'path has '+ str(len(self.contentList))+' enteries'
        print 'path xy has length of %fmm'%lxy

    def clean(self):
        '''clean up

        remove two following lines that are the same
        '''
        for i in range(len(self.contentList)-1):
            p=self.contentList[i]
            q=self.contentList[i+1]
            l=[]
            if p[0]==q[0] and p[1]==q[1]:
                #del self.contentList[i+1]
                print 'removed an item'
                pass
            else:
                l.append(p)

        self.contentList=l


    def splitHorizVert(self,vert=True,horiz=True):
        '''find horizontal and vertical parts
        returns the original Double a list of Doubles
        which was split up at horizontal and vertical
        coordinates
        '''
        if vert:
            print 'splitting vertical'
        if horiz:
            print 'splitting horizontal'
        parts=[]
        part=[]
        dxnew=0
        dynew=0
        dxold=self.contentList[1][0]-self.contentList[0][0]
        dyold=self.contentList[1][1]-self.contentList[0][1]
        for i in range(1,len(self.contentList)): #1 bis len-1
            p=self.contentList[i]
            q=self.contentList[i-1]
            dxnew=p[0]-q[0]
            dynew=p[1]-q[1]
            #print str(i) + ' ' +str(dxnew) + ' ' +str(dynew)
            if vert and (dxnew==0 or dxold<0 and dxnew>0 or dxold>0 and dxnew<0 ):
                print "sign change of x at %i , x:%f y=%f"%(i,p[0],p[1])
                part.append(p)
                parts.append( Double(part))
                part=[]
            if horiz and (dynew==0 or dyold<0 and dynew>0 or dyold>0 and dynew<0):
                print "sign change of y at %i , x:%f y=%f"%(i,p[0],p[1])
                part.append(p)
                parts.append( Double(part))
                part=[]
            part.append(p)
            dxold=dxnew
            dyold=dynew
        parts.append( Double(part))
        print 'returning %i sub-Doubles'%(len(parts))
        return parts

    def splitHoriz(self):
            return self.splitHorizVert(vert=False,horiz=True)
    def splitVert(self):
            return self.splitHorizVert(vert=True,horiz=False)

    def appendRelativePath(self):
        """implement me"""
        pass
    def appendRelativeItem(self):
        """implement me"""
        pass



class QuadColumn( PathContainer ): #[x,y,u,v]
    """A List with four values -> four columns

    Represents a synchronized cut Path.
    Alias name is Quad
    """

    def __init__(self,arg):
        PathContainer.__init__(self,arg)
        if self.debug:
            print '-------------------'
            print 'creating QuadColumn'
        self.paramList=[]
        self.cutTime=0      # in scaled system Time

#ugly intendation here (fixme)
    def toNCFile_xyuv(self,filename):
        """writes a quad to a NC file
        """
        f=file(filename,'w+')
        os.chmod(filename,0777) # octal nubmer -> leading 0
        print( '\nWriting NC data to "' +filename+'"')

        #f.write("G21 (Millimeters)\n")
        #f.write("F500 (FeedRate)\n")
        #f.write("G64 P0.001    (Digital Output Control???)\n")
        #f.write("G0 X0 Y0 U0 V0\n" )
        #f.write("(MSG, Press S)\n")
        #f.write("M00 (Pause - > Press S)\n")

        #fits to lotar sammels paeamble
        f.write("(test XY UV emc2 4 achsen zaggi)\n")
        f.write("G17.1 G21 G90 G61 G54\n")
        f.write("(AXIS,XY_Z_POS,5)\n")
        f.write("(AXIS,UV_Z_POS,100)\n")
        f.write("(AXIS,GRID,5)\n")
        f.write("G0 x0 y0 u0 v0\n")
        f.write("G0 x5 y5\n")
        for i in range(len(self.contentList)):
            p=self.contentList[i]
            s="G1 X%f Y%f U%f V%f"%(p[0],p[1],p[2],p[3])
            if i==0:
                s+=" F500"
            f.write(  s +'\n')
        f.write("G0 x0 u0\n")
        f.write("G0 x0 y0 u0 v0\n")
        f.write("M30\n")
        f.close()
        print( 'wrote data file "' +filename+'" with ' + str (len(self.contentList)) +' enteries')

    def plView(self):
        """use pyton lab viewer to open the quad
        """
        x=[]
        y=[]
        u=[]
        v=[]

        for i in self.contentList:
            x.append(i[0])
            y.append(i[1])
            u.append(i[2])
            v.append(i[3])


        pylab.xlabel('xu')
        pylab.ylabel('yv')
        pylab.title('Quad Plot xy:green uv:red')
        pylab.plot(x, y,'g.', u, v,'m.')
        #ylim(-1,4)
        #yticks(arange(4), ['S1', 'S2', 'S3', 'S4'])
        pylab.grid(True)
        pylab.show()

    def ViView(self,arg=None):
        """Python Visual Viewer
        be warned due to an unkown problem ViView exits the comlete session after closing.
        """
        VisualViewerQuad(self.contentList,arg=arg)

    def splineInt(self,amount=1000,smooth=0,degree=3):
        """returns a new interpolated Quad with equalized
        point density using "amount=..." points

        to use a linear interpolation provide degree=1 as parameter
        """
        print 'old length %i' %self.getListSize()
        self.clean()
        p=QuadPathApproximation(self.contentList,amount,smooth=smooth,degree=degree)
        a= p.approximate()
        #print a
        q=QuadColumn(a)
        print 'new length %i' %q.getListSize()
        return q

    #self.splineApprox=self.splineInt

    def addOffset(self,x,y,u,v):
        """Shift: Add an constant offset"""
        for i in range(len(self.contentList)):
            p=self.contentList[i]
            self.contentList[i]=( p[0]+x, p[1]+y,p[2]+u,p[3]+v )

        #map( lambda x:x+1 , [1,2,3,4,5])
        #map( lambda (x,y,u,v):(x+o,y+o,u+o,v+o)  , [ (1,2,3,4) ,(2,3,4,5)])

    def setOffset(self,x,y,u,v):
        """shifts the begin of the quad to offset
        """
        p=self.contentList[0]

        self.addOffset( x-p[0],y-p[1],u-p[2],v-p[3])


    def setZeroOffset(self,side=None):
        """shifts the begin of the quad to zero,
        it can also shift only one side
        Parameters:
        side= [ none | 'xy' | 'uv'| 'xyuv' ]
        """
        p=self.contentList[0]

        if side=='uv':
            self.addOffset( -p[2],-p[3],-p[2],-p[3])
        elif side=='xyuv':
            p=self.contentList[0]
            self.addOffset( -p[0],-p[1],-p[2],-p[3])
        elif side=='xy' or side==None:
            p=self.contentList[0]
            self.addOffset( -p[0],-p[1],-p[0],-p[1])
        else:
            print 'wrong parameter'

    def appendRelativePath(self):
        """implement me"""
        pass
    def appendRelativeItem(self):
        """implement me"""
        pass


    def scale(self,xs,ys,us,vs):
        """
        Scale the Double with in x,y,u and v direction.
        """

        for i in range(len(self.contentList)):
            p=self.contentList[i]
            self.contentList[i]=( p[0]*xs, p[1]*ys,p[2]*us,p[3]*vs )

    def mirror_xu(self,x,u):
        '''mirror vertical at x and u
        mirror_xu(0,0) : mirror vertically at zero
        '''
        #xnew=2 xplane -xold
        for i in range(len(self.contentList)):
            p=self.contentList[i]
            self.contentList[i]=(   2*x-p[0], p[1],
                                2*u-p[2], p[3] )


    def mirror_yv(self,y,v):
        '''mirror horizontal planes at y and v
        mirror_yv(0,0) : mirror horizontally at zero
        '''
        #xnew=2 xplane -xold
        for i in range(len(self.contentList)):
            p=self.contentList[i]
            self.contentList[i]=(   p[0], 2*y-p[1],
                                p[2], 2*y-p[3] )

    def mirrorUnits(self):
        '''mirror units

        flip xy and uv
        '''
        for i in range(len(self.contentList)):
            p=self.contentList[i]
            self.contentList[i]=(   p[2], p[3],
                                p[0], p[1] )

    def displace(self,dxy,duv):
        '''displace path : Burn away correction ....

        returns the new quad
        test version!!!
        Better Algorithm needed.
        -makes problems at small diameters that should be shifted to inside
        -begins to displace in the direction that is at the top if a positive displacement is choosed
        '''
        print 'test version'
        disp=[]
        for i in range(len(self.contentList)-1):
            p=self.contentList[i]
            r=self.contentList[i+1]

            kx=r[0]-p[0]    #vector from the old to the new
            ky=r[1]-p[1]

            ku=r[2]-p[2]    #vector from the old to the new
            kv=r[3]-p[3]
            #print i

            if ky!=0 and kx!=0 :    #create a perpendicular vector to k
                if ky>0:    sx=1
                else:   sx=-1
                sy=-sx*kx/ky # s*k=0

            elif kx==0:
                if ky>0:    sx=1
                else:   sx=-1
                sy=0
                print 'delta value zero: kx '+str(i) +' ' + str(kx)+' '+str(ky) +' '+str(sx)+ ' '+str(sy)
            elif ky==0:
                if kx>0:    sy=-1
                else:   sy=1
                sx=0
                print 'delta value zero: ky '+str(i) +' ' + str(kx)+' '+str(ky) +' '+str(sx)+ ' '+str(sy)
            else:
                print 'catch me xy'
                print i
                sys.exit()

            if ku!=0 and kv!=0 :    #create a perpendicular vector to k
                if kv>0:    su=1
                else:   su=-1
                sv=-su*ku/kv # s*k=0

            elif ku==0:
                if kv>0:    su=1
                else:   su=-1
                sv=0
                print 'delta value zero: ku '+str(i) +' ' + str(ku)+' '+str(kv) +' '+str(su)+ ' '+str(sv)

            elif kv==0:
                if ku>0:    sv=-1
                else:   sv=1
                su=0
                print 'delta value zero: kv '+str(i) +' ' + str(ku)+' '+str(kv) +' '+str(su)+ ' '+str(sv)

            else:
                print 'catch me xy'
                print i
                sys.exit()



            l= math.sqrt( sx*sx+sy*sy)
            sxd=dxy* sx/l   # normalize it and multiply with d
            syd=dxy* sy/l

            l= math.sqrt( su*su+sv*sv)
            sud=duv* su/l   # normalize it and multiply with d
            svd=duv* sv/l

            pold=self.contentList[i]

            #self.contentList[i]=(p[0]+sxd,p[1]+syd,p[2]+sud,p[3]+svd)
            disp.append ( (p[0]+sxd,p[1]+syd,p[2]+sud,p[3]+svd) )


        #dirty repeats the last one
        #self.contentList[-1]=(p[0]+sxd,p[1]+syd,p[2],p[3])
        disp.append( (p[0]+sxd,p[1]+syd,p[2]+sud,p[3]+svd) )

        print 'dirty boy'
        return QuadColumn(disp)




    def rotate(self,xorgin,yorgin,uorgin,vorgin,xyangle,uvangle):
        """rotate around the given orgins
        """
        xya=math.pi/180*xyangle
        uva=math.pi/180*uvangle
        cos=math.cos
        sin=math.sin
        for i in range(len(self.contentList)):
            p=self.contentList[i]
            nx=(p[0]-xorgin)*cos(xya)- (p[1]-yorgin)*sin(xya) + xorgin
            ny=(p[0]-xorgin)*sin(xya)+ (p[1]-yorgin)*cos(xya)   + yorgin

            nu=(p[2]-uorgin)*cos(uva)- (p[3]-vorgin)*sin(uva) + uorgin
            nv=(p[2]-uorgin)*sin(uva)+ (p[3]-vorgin)*cos(uva) + vorgin

            #p[0],p[1],p[2],p[3]=nx,ny,nu,nv
            self.contentList[i]=(nx,ny,nu,nv)

    def oldView(self):
        """uses the built in viewer to open the quad
        """
        print len (self.contentList)
        if len (self.contentList)>=1:
            viewer = Viewer(self.getContentList(),'quad')
            viewer.main()

    def view(self):
        """uses the NEW viewer to open the quad
        """
        print len (self.contentList)
        if len (self.contentList)>=1:
            tempfile='Shapes/path.temp.quad'
            self.writeToFile(tempfile)
            path='..'+os.sep+'FoamBladeViewer'+os.sep+'fbv.py'
            #path='.'+os.sep+'dxf2quad'+os.sep+'TkInterface.py '    #for fbv releases
            try:
                execstring=sys.executable +' '+path+' '+tempfile
                print execstring
                os.system(execstring)
                #os.system("rxvt -e "+path+args)
            except:
                #print 'fbv.py: Couldn\'t open file'
                self.MessageError('Failure in Calling the fbv.py',self.window)
                return None

    def itemToString(self,item):
        s=str(round(item[0],4))+self.sep+str(round(item[1],4))+self.sep+\
            str(round(item[2],4))+self.sep+str(round(item[3],4))
        return s

    def stringToItem(self,s):
        if re.match(  '^(\s*-?\d+(\.\d+)?){4}',s):
            p=re.split('\s*',s,4)
            while '' in p:
                p.remove('')

            return [ float(p[0]),float ( p[1]),float(p[2]),float ( p[3])]
        else :
            return None

    #def modifyQuadColumn(self):
    #   #modifies the quadcolumn
    #   pass

    def interpolateLinearDummy(self):
        #returns a steplist
        #dummy
        stepList=[]
        for i in self.contentList:
            stepList.append( [ int(i[0]),int(i[1]),int(i[2]),int(i[3] )])

        step=StepList(stepList)
        return step

    def interpolateLinear(self):
        """test it/fix it
        do not use directly
        """

        li=LinearInterpolation(self.contentList,v_max=1,pathfreq=0)
        #  1mm/s and 1000 values per second
        quad=QuadColumn(li.interpolateLinear())
        return quad

    def clean(self):
        '''clean up

        remove two following lines that are the same
        '''
        l=[]
        for i in range(len(self.contentList)-1):
            p=self.contentList[i]
            q=self.contentList[i+1]

            if p[0]==q[0] and p[1]==q[1] and p[2]==q[2] and p[3]==q[3] :
                #del self.contentList[i+1]
                if self.debug: print 'removed item'
                pass
            else :
                l.append(p)
        l.append(q)
        self.contentList=l



    def convertToStepList(self,mach):
        """returns an interpolated StepList in machine resolution
        """
        res=mach.getResolution()
        print 'creating StepList from QuadColumn'
        if self.debug:
            print 'This could take a while'
        if self.debug:
            print 'using Resoution: ' +str(res)
            print 'using Path Frequency: ' +str(mach.getPathFreq())
            print 'using Pathspeed: ' +str(mach.getMaxSpeed())

        li=LinearInterpolation(self.contentList,mach.getMaxSpeed(),mach.getPathFreq())

        cuttime=li.getDuration()#Time needed to cut with max Speed

        xpos,ypos,upos,vpos=mach.getPosition()  #   cut unit position [steps]
        if self.debug:
            print 'current (unused)unit position: ' +str(mach.getPosition())#se below

        x=y=u=v=0           #   path position       [mm]
        dx=dy=du=dv=0           #difference between cut unit and path   [steps]

        dxmax=dxmaxstep=0   #maximum difference between unit ant path
        dymax=dymaxstep=0
        dumax=dumaxstep=0
        dvmax=dvmaxstep=0
        limit=1
        #limit=2

        #lets assume that the unit of cuttime is seconds
        overallsteps=   mach.getPathFreq() * cuttime #freq* cuttime=    1/s*s
        freq=mach.getPathFreq()
        if self.debug: print 'using :'+str(overallsteps) + ' steps'

        #cut unit pos for t=0
        xt,yt,ut,vt=li.getInterpolatedValue(0)

        #xpos,ypos,upos,vpos=xt*res,yt*res,ut*res,vt*res
        xpos=res*xt
        ypos=res*yt
        upos=res*ut
        vpos=res*vt
        mach.setPosition( (xpos,ypos,upos,vpos) )
        if self.debug:
            print 'new unit position: ' +str(mach.getPosition())
        #print xpos

        l=[]
        l.append((int(xpos),int(ypos),int(upos),int(vpos)))
        if self.debug:
            print 'beginning to interpolate.'
        print '   0%',
        for scaled_t in range(1, 1+int(round(overallsteps))):
            # +1 for the last one; but it is to big -> extension in Algorithms
            #if self.debug:

            if (scaled_t%1000) ==0:
                p=1.0*scaled_t/(1+int(round(overallsteps)))
                #print '\b\b\b\b\b'+ str( round(100* p)) + '%',
                #not that nice
                #print p
                #print FoamBlade_GUI
                #if FoamBlade_GUI:
                #print 'GUI : ' + str(self.getGUI())
                if self.getGUI():
                    gui=self.getGUI()
                    #print 'setting bar to ' + str(FoamBlade_GUI)
                    #FoamBlade_GUI.set_generate_progressbar(p)

                    gui.generateThread.update_generate_progressbar(p*6.0/10)

                    #we will need a thread for this
                else:
                    print str( round(100* p)) + '%\n',
                    sys.stdout.flush()


            #try to follow the path with the cut units
            t=float(scaled_t)/freq
            #print t
            x,y,u,v = li.getInterpolatedValue(t)

            dx=float(x*res -xpos)
            dy=float(y*res -ypos)
            du=float(u*res -upos)
            dv=float(v*res -vpos)

            if dx > dxmax:
                dxmax=dx
                dxmaxstep=t
            if dy > dymax:
                dymax=dy
                dymaxstep=t
            if du > dumax:
                dumax=du
                dumaxstep=t
            if dv > dvmax:
                dvmax=dv
                dvmaxstep=t
            #print dx
            if dx > limit :
                xpos+=1     #increase u to minimize the difference
            elif dx < -limit :
                xpos-=1
            #elif -limit< du <limit :
            else:
                pass

            if dy > limit :
                ypos+=1
            elif dy < -limit :
                ypos-=1
            #if -limit< dv <limit :
            else:
                pass

            if du > limit :
                upos+=1     #increase u to minimize the difference
            elif du < -limit :
                upos-=1
            #if -limit< du <limit :
            #   upos=0
            else:
                pass

            if dv > limit :
                vpos+=1
            elif dv < -limit :
                vpos-=1
            #if -limit< dv <limit :
            #   dirv=0
            else:
                pass

            l.append((int(xpos),int(ypos),int(upos),int(vpos)))

        if self.debug:
            print '\nfinished creating the StepList'
        s=StepList(l)
        if self.debug:
            s.writeToFile('Shapes/path-debug.step')
        mach.setPosition( (xpos,ypos,upos,vpos) )
        if self.debug:
            print 'current unit position: ' +str(mach.getPosition())
            print 'maximum difference between path and cut unit x:%f y: %f u:%f v:%f'\
                %( dxmax,dymax,dumax,dvmax)
        deplimit=3
        if max(dxmax,dymax,dumax,dvmax) >= deplimit:
            print '     ---WARNING DEPRECATION OF PATH IS POSSIBLE---'
            print 'contact the Author if you haven\'t played with the virtual speeds'

        return s

    def getLength(self):
        """return Length of both pathes
        """

        pold=self.contentList[0]
        lxy=0
        luv=0
        for i in range(len(self.contentList)): #-1
            p=self.contentList[i]
            lxy+= math.sqrt( (p[0]-pold[0])**2+(p[1]-pold[1])**2)
            luv+= math.sqrt( (p[2]-pold[2])**2+(p[3]-pold[3])**2)
            pold=p

        print 'path has '+ str(len(self.contentList))+' enteries'
        print 'path xy has length of %fmm'%lxy
        print 'path uv has length of %fmm'%luv


    def getDimension(self):
        """returns the object sizes in mm
        """
        p=self.contentList[0]
        xmax=xmin=p[0]
        ymax=ymin=p[1]
        umax=umin=p[2]
        vmax=vmin=p[3]


        for i in range(len(self.contentList)): #-1
            p=self.contentList[i]
            if xmax<p[0]:
                xmax=p[0]
            if xmin>p[0]:
                xmin=p[0]

            if ymax<p[1]:
                ymax=p[1]
            if ymin>p[1]:
                ymin=p[1]

            if umax<p[2]:
                umax=p[2]
            if umin>p[2]:
                umin=p[2]

            if vmax<p[3]:
                vmax=p[3]
            if vmin>p[3]:
                vmin=p[3]

        objectx=xmax+abs(xmin)
        objecty=ymax+abs(ymin)
        objectu=umax+abs(umin)
        objectv=vmax+abs(vmin)

        objectxu=max( xmax,umax) + abs( min( xmin,umin))
        objectyv=max( ymax,vmax) + abs( min( ymin,vmin))

        print 'object size: ' + str(objectx) + ',  ' + str(objecty)+ ',  '+  str(objectu) + ',  ' + str(objectv)
        print 'overall both sides: ' + str(objectxu) + ',  ' + str(objectyv)
        #return ( objectx,objecty,objectu,objectv)

    def convertToDoubles(self):
        """returns two doubles

        call:  d1,d2=q.convertToDoubles()
        """
        d1=[]
        d2=[]
        for i in self.contentList :
            d1.append( (i[0],i[1]) )
            d2.append( (i[2],i[3]) )

        return DoubleColumn(d1),DoubleColumn(d2)

    def repeatQuad(self, n, x,y,u,v):
        """repeats the quad n times by adding the offset xyuv every time
        the new quad is returned
        """
        r=Quad('empty')
        for i in range(n):
            o=Quad(self.contentList)
            o.addOffset(i*x,i*y,i*u,i*v)
            r.appendPath(o)

        return r

## end of the Quad Class
## a few functions that return elemental Quads

def notches (width,depth,num):
    """creates num notches that are with away from each other and
    with depth of depth.
    returns Path as Quad
    """
    l=[]
    l.append((0,0,0,0))
    for i in range(num):
        l.append((i*width,-depth,i*width,-depth))
        l.append((i*width,0,i*width,0))
        l.append((width*(i+1),0,width*(i+1),0))

    l.append((width*num,-depth,width*num,-depth))
    l.append((width*num,0,width*num,0))

    return Quad(l)

def circle(radius,steps):
    """returns a circle with radius radius and steps steps on its circumference
    returns a Quad
    """
    T=2*math.pi/steps
    l=[]
    for t in range(steps+1):
        c=radius*math.cos( T*t)
        s=radius*math.sin( T*t)
        l.append((c,s,c,s))
    return Quad(l)

def scanDXF_File(filename):
    """uses dxf2quad to get a quad via the file interface"""
    ##not very beautiful
    os.system(' rm -f Shapes/path.temp')
    path='../dxf2quad/dxf2quad.py '
    args= filename+ ' Shapes/path.temp'
    try:
        os.system(path+args)
        #os.system("rxvt -e "+path+args)
        #os.system("gnome-terminal -e "+path+args)
    except:
        return None
    return Quad('Shapes/path.temp')

def scanDXF(filename):
    """uses dxf2quad to get a quad"""
    sys.path.append("../dxf2quad/")
    import dxf2quad
    
    #def dxf2quad(filename,outfile,ppmm,debug):
    ppmm=10
    debug=0
    outfile=None
    quad=dxf2quad.dxf2quad(filename,outfile,ppmm,debug)
    
    return Quad(quad)

#####


class StepList(PathContainer):
    """equal to a Quads, but contains absolute motor steps
    """

    def __init__(self,arg):
        PathContainer.__init__(self,arg)
        if self.debug:
            print '-------------------'
            print 'creating Steplist'

    def view(self):
        """not recommended

        because StepLists can be very big
        """
        viewer = Viewer(self.getContentList(),'quad')
        viewer.main()

    def itemToString(self,item):
        s=str(item[0])+self.sep+str(item[1]) +self.sep+str(item[2])+self.sep+str(item[3])
        return s

    def stringToItem(self,s):
        if re.match('^\s*-?\d+\s*-?\d+\s*-?\d+\s*-?\d+',s):
            p=re.split('\s*',s,4)
            #print p
            while '' in p:
                p.remove('')
            #print p
            k= [ int(p[0])  , int ( p[1]),int(p[2]) ,int ( p[3])]
            #print k
            return k
        else :
            return None

    def convertToCutPath(self,mach):
        """returns a CutPath
        """
        machine=mach
        self.style=machine.getStyle()
        if self.debug:
            print 'creating CutPath from StepList'
        pathList=[]

        #print 'beginning at zero'
        #oldStep=(0,0,0,0)
        step=self.contentList[0]
        oldStep=(step[0],step[1],step[2],step[3])
        machine.getStepByte=machine.getStepByte

        for step in self.contentList[1:] :
            diff=( step[0]-oldStep[0],step[1]-oldStep[1],
                    step[2]-oldStep[2],step[3]-oldStep[3])
            #print 'diff: ' +str(diff)
            if abs(diff[0])>1 or abs(diff[1])>1 or abs(diff[2])>1 or abs(diff[3])>1 :
                print 'error in Steplist: ' + str(step)
                sys.exit()
            b=machine.getStepByte(diff)#def above
            #print 'byte: ' +str(b)
            pathList.append(b)
            oldStep=step

        path=CutPath(pathList)
        return path

    def loadHotWire2(self,mach):
        """converts the steplist to a format that is loadable in hotwire2
        rg15 45948 packaged into 8078
        """
        machine=mach
        self.style=machine.getStyle()
        if self.debug:
            print 'converting steplist to a format that is loadable in hotwire2'
        pathList=[]

        #print 'beginning at zero'
        #oldStep=(0,0,0,0)
        step=self.contentList[0]
        oldStep=(step[0],step[1],step[2],step[3])
        machine.getStepByte=machine.getStepByte

        olddiff=[0,0,0,0]
        diffcount=0

        #this file is loadable from the debug gui
        outputfile=file('./output.hw2','w')
        outputfile.write('\nd=1000;\n\n')

        i=0

        for step in self.contentList[1:] :
            diff=( step[0]-oldStep[0],step[1]-oldStep[1],
                    step[2]-oldStep[2],step[3]-oldStep[3])
            #print 'diff: ' +str(diff)
            if abs(diff[0])>1 or abs(diff[1])>1 or abs(diff[2])>1 or abs(diff[3])>1 :
                print 'error in Steplist: ' + str(step) + '  ' + str(oldStep)
                sys.exit()

            #print 'diff %s'% str(diff)
            #print 'olddiff %s'%str(olddiff)

            if diff[0]==olddiff[0] and diff[1]==olddiff[1] and\
                diff[2]==olddiff[2] and diff[3]==olddiff[3]:
                diffcount+=1;
            else:
                #append the last
                b=machine.getStepByte(olddiff)#def above
                #print str(i)+' byte: ' +str(b) + ' repeat ' + str(diffcount)
                #pathList.append( (b,diffcount) )
                outputfile.write('m=%i;\nr=%i;\n'%(b,diffcount))
                olddiff=diff[:]
                diffcount=0;
                i+=1;
            oldStep=step

        #write the last
        b=machine.getStepByte(olddiff)#def above
        outputfile.write('m=%i;\nr=%i;\n'%(b,diffcount))
        i+=1;


        print "generated %i loadable steps" %i

        outputfile.write('# End \n')
        outputfile.close()
        #path=CompressedCutPath(pathList)
        #return path

    def analyze(self):
        """calculate several metrics"""

        first=self.contentList[0]
        last=self.contentList[-1]
        diff=( first[0]-last[0],first[1]-last[1],
                    first[2]-last[2],first[3]-last[3])

        print 'overall end point  difference:'
        print "dx: %i\ndy: %i\ndu: %i\ndv %i"%(diff[0],diff[1],diff[2],diff[3])

##  for i in range(len(self.contentList)): #-1
##      dx,dy,du,dv=0,0,0,0
##      p=self.contentList[i]
##          dx+=p[0]
##          dy+=p[1]
##          du+=p[2]
##          dv+=p[3]

##      print 'overall step difference:'
##      print "dx: %i\ndy: %i\ndu: %i\ndv %i"%(dx,dy,du,dv)


class CutPath(PathContainer):
    """only one columns that contains bytes
    """
    def __init__(self,arg):
        PathContainer.__init__(self,arg)
        if self.debug:
            print '-------------------'
            print 'creating CutPath'
        self.debug=False

    def itemToString(self,item):
        #print item
        #s=str(hex(item))
        s= '%X'%item
        #print s
        return s

    def stringToItem(self,s):
        #untested
        if re.match('^\d\d',s):
            k=hex(int(s,16))
            #print k
            return k
        else:
            return None

Quad=QuadColumn
Double=DoubleColumn





