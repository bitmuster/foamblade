############################################################################
#    Copyright (C) 2007-2012 by Michael Abel                               #
#    mabel@quasiinfinitesimal.org                                          #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

import math

##class InterpolationAlgorithm(object)
##  """2D interpolation algorithms class of (x(t),y(t)) pathes.
##
##  The Constructor creates a parameter list that is
##  later used to calculate points on the path.
##  """
##
##  def getDuration(self):
##      """returns the time needed to go through the interpolated path"""
##      print 'This operation is abstract'
##      sys.exit()
##
##  def getLength(self):
##      """returns the length the interpolated path"""
##      print 'This operation is abstract'
##      sys.exit()
##
##  print 'Tried to make an object form an abstract class'
##      sys.exit()
##


class LinearInterpolation(object):
#class LinearInterpolation(InterpolationAlgorithm):

    def getDuration(self):
        return self.duration

    def createParamList(self):

        #creates a list of such lines
        #each line contains values that help to interpolate a section
        #a section is between two points

        #conains also the last one

        #newRow=(   (x,y,u,v),          start coordinates of the section
        #           (dx,  dy,  du,  dv),    delta values from the start coordinates to the end points
        #           lxy, luv, vxy, vuv,     the length of a section and the desired speed to cut with syncronized axis
        #           T,   t )                duration of this section, start time of the section


        #12 from one to two
        #dz=zi-zj
        #l_xy12=sqrt( dx^2+dy^2)
        #l_uv12=
        #v_xy12 if lxy is longer-> v_xy=self.v_max
        #->v_uv=luv/lxy * self.v_max

        #   v = l / T  ; length units to cut per Time units

        t=0         #unit?

        lastrow=self.contentList[0]

        last_T=0

        Lxy=0   #Path length
        Luv=0

        for row in self.contentList[1:]:
            if self.debug:
                print 'row: ' + str(row)
            dx=row[0]-lastrow[0]
            dy=row[1]-lastrow[1]
            du=row[2]-lastrow[2]
            dv=row[3]-lastrow[3]

            lxy=math.sqrt( dx**2+dy**2)
            luv=math.sqrt( du**2+dv**2)


            if lxy==luv:
                vxy=self.v_max
                vuv=self.v_max
                T=lxy/self.v_max
            elif lxy>luv:
                vxy=self.v_max
                vuv= luv/lxy * self.v_max
                T=lxy/self.v_max
            else:
                vuv=self.v_max
                vxy= lxy/luv * self.v_max
                T=luv/self.v_max
            t+=last_T
            Lxy+=lxy
            Luv+=luv

            newRow=( (lastrow[0],lastrow[1],lastrow[2],lastrow[3]),
                    (dx,  dy,  du,  dv),
                    lxy, luv, vxy, vuv, T,   t )

            #print str(newRow ) +' '
            #print str(T)+' '+str(t)
            self.paramList.append(newRow)
            last_T=T
            lastrow=row
        if self.debug:
            print self.paramList
        self.duration=t+T
        if self.debug:
            print 'created interpolation parameter "search" list with '+ str ( len(self.paramList)) +' enteries'
        print "\tTime to go through path : %f in Time units" % (self.duration)
        print '\tPath length xy: '+str(Lxy) +' length uv ' + str(Luv)+ ' in length units'



    def getInterpolatedValue(self,time):
        #print 'time ' + str(time)
        extension=0.1
        assert(time<=self.duration+extension)
        def     evaluate( num=0, side=0 ): #side xy=0

            # x(t) = xi + (xj-xi) * vx (t-ti) / lxy
            # if lxy ==0 -> vxy==0
            if self.paramList[self.index][4+side] ==0 \
                    or self.paramList[self.index][2+side] ==0 : # speed or length
                val=self.paramList[self.index][0][num]
                return val

            else:
                #the formula in the docu
                val = self.paramList[self.index][0][num] +  \
                    self.paramList[self.index][1][num]  *   \
                    self.paramList[self.index][4+side]  *   \
                    ( time-self.paramList[self.index][7]) / \
                    self.paramList[self.index][2+side]

                if self.debug:
                    print 'outdated'
                    print 'val= '+ str( self.paramList[self.index][0][num]) +'  + ' \
                        + str( self.paramList[self.index][1][num] ) +' * ' +\
                        str(self.paramList[self.index][4+side]) +' * ( ' +str(time) + ' - ' +\
                        str(self.paramList[self.index][7]) +') '+' / ' \
                        + str (self.paramList[self.index][2+side]) + ' ) = ',
                    print str(val) +' index:' + str (self.index)
                return val

        if self.debug:
            print ''

        if len( self.paramList)==0: #occurs in handcontrol (single path)
            self.index=0
        else:
            #try the last used
            if  (self.index < ( len( self.paramList)  -1 )) and \
                    (self.paramList[self.index][7] <= time < self.paramList[self.index+1][7] ):
                    #print 'using old index'
                #print 'o',
                pass
            #if there is one: try the next
            elif (self.index< (len( self.paramList)-2) ) and \
                        (self.paramList[self.index+1][7] <= time < self.paramList[self.index+2][7]):
                    #print 'using next index'
                    self.index+=1
                    #print '+',
                    pass

            #try the last one
            elif (self.index== (len( self.paramList)-1) ):
                self.index=len( self.paramList)-1
                #print 'l',

            #search it
            else:
                #search it
                #print 'len paramList: %i' % (len(self.paramList))
                for i in range( len( self.paramList)): #tuneable
                    #searching for the right index
                    #print 'new index'
                    if self.paramList[i][7] > time:
                        #print 'n',
                        self.index=i-1  #it was the last one
                        #print 'searched ' + str(time)
                        #print 'old: ' + str(self.paramList[i-1][7])
                        #print 'new: '+ str(self.paramList[i][7])
                        #print 'index: ' + str(index)
                        assert( self.paramList[i-1][7] <= time < self.paramList[i][7] )
                        break
                    if  i==len( self.paramList)-1: #-1:     #pach reached the last in list
                        print "warning: last one wasn't recognized in the tuned search "
                        self.index=i
                        break

        if self.debug:
            print 'searched ' + str(time)
            print 'old: ' + str(self.paramList[self.index-1][7])
            print 'new: '+ str(self.paramList[self.index][7])
            print 'index: ' + str(self.index)

        x=evaluate(0,0)
        y=evaluate(1,0)
        u=evaluate(2,1)
        v=evaluate(3,1)
        return (x,y,u,v)

    def interpolateLinear(self):
        #returns a quad touple
        qList=[]

        print 'beginning to interpolate. Estimated Length of new Path : '+str(int (self.cutTime*self.factor))

        for i in range( 0, int (self.cutTime*self.factor) ):
            if self.debug:
                print str(i) + '  ' + str(float(i)/self.factor)
            qList.append ( self.getInterpolatedValue(float(i) /self.factor,False))

        #last one
        qList.append ( self.getInterpolatedValue(self.cutTime ,False))

        return qList




    def __init__(self,contentList,v_max,pathfreq):
        """
        needs
        the maximum used speed
        and the pathfreq = value density per time unit
        """
        self.paramList=[]
        self.contentList=[]
        self.contentList[:]=contentList
        self.duration=0
        self.index=0
        #   v = speed of the Path
        #   v = l / T  ; length units to cut per Time units
            #use virtual or real units:
        # -> 100 l in one T -> v=100
        #   T=1s l=1mm -> v=1

        self.v_max=v_max
            #maximum speed to use
            #the axe with the longer path uses v_max
        self.factor=pathfreq
            #value density per time unit T -> if you want the cut freq

        self.debug=False
        self.createParamList() #sets Time





