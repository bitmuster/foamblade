 
Foam Blade Suite - Subset
=========================

WARNING: Mixture of python2 and python3, beware

Released under the GPL; Michael Abel (C) 2007-2020

This is the FoamBladeSuite, a framework to support cut path generation
for 4 axis hot wire foam-cutting machines. 

This repository is used to continue development of the FoamBladeSuite at 
http://foamblade.sourceforge.net/ and gitorious.org/foambladesuite/foambladesuite.git.

Ongoing work aims towards support of CAM functionality and cut path generation.

Functionality for driving hot-wire cutting machines was removed as EMC 
(www.linuxcnc.org) is far better suited for this task.

The original project is located at: http://foamblade.sourceforge.net/

Versions
--------

0.4 2008
0.5 2021 beta !!!

Folders
-------

dxf2quad        : Conversion of DXF files into Quad and G-Code representation
FoamBladeViewer : Visualization of Quad files
EMC/EMC-Configs : Valid configurations for EMC
EMC/NC-Files    : G-Code test files

Example Session
---------------

git clone git://gitorious.org/foambladesuite/foambladesuite.git

cd foambladesuite/dxf2quad/

# Generate cut files from the test file allObjects.dxf
# This generates a .quad file and an .ngc file for EMC which uses 
# axes x,y,u and v.

python3 dxf2quad.py paths/allObjects.dxf paths/allObjects.quad

# Analyze the result with the Viewer and Simulator

python3 ../FoamBladeViewer/fbv.py paths/allObjects.quad



